LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
  
LOCAL_MODULE_TAGS := optional
  
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_RESOURCE_DIR := $(addprefix $(LOCAL_PATH)/, res)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES :=clock-iwds-jar:libs/iwds-jar.jar
include $(BUILD_MULTI_PREBUILT)

  
LOCAL_STATIC_JAVA_LIBRARIES := \
      clock-iwds-jar  \
      android-support-v4 \
  
LOCAL_PACKAGE_NAME := com.ingenic.library.clock
  
LOCAL_PROGUARD_ENABLED := disabled
  
include $(BUILD_PACKAGE)
  
  # Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))

