/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  foror-fighter/elf/AmazingClock project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.library.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ingenic.library.analog.AnalogClockView;
import com.ingenic.library.clock.Clock;
import com.ingenic.library.digital.DigitalClockView;

/**
 * clock adapter
 *
 * @author ShiGuanghua(kenny)
 *
 */
public class ClockViewAdapter extends BaseAdapter {

    private List<Clock> mClockList = null;

    private Context mContext;
    private static final int CLOCK_DIGITAL_TYPE = 0;
    private static final int CLOCK_ANALOG_TYPE = 1;
    private static final int CLOCK_TYPE_COUNT = 2;

    /**
     * 构造方法
     * 
     * @param context
     *            上下文
     * @param list
     *            所有APP的集合
     * @param page
     *            当前页
     */
    public ClockViewAdapter(Context context, List<Clock> clockList) {
        mContext = context;
        mClockList = clockList;

    }

    @Override
    public int getViewTypeCount() {
        return CLOCK_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {

        if (Clock.TYPE_DIGITAL.equals(mClockList.get(position).getmType())) {
            // 数字表盘
            return CLOCK_DIGITAL_TYPE;
        } else if (Clock.TYPE_ANALOG
                .equals(mClockList.get(position).getmType())) {
            // 模拟表盘
            return CLOCK_ANALOG_TYPE;
        }
        return CLOCK_DIGITAL_TYPE;
    }

    @Override
    public int getCount() {

        return mClockList.size();
    }

    @Override
    public Object getItem(int arg0) {

        return mClockList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        final ViewHolder mViewHolder;
        final Clock mClock;
        final int clockType = getItemViewType(position);
        mClock = mClockList.get(position);
        if (contentView == null) {
            mViewHolder = new ViewHolder();
            switch (clockType) {
            case CLOCK_DIGITAL_TYPE:
                // 数字表盘
                mViewHolder.mDigitalClockView = new DigitalClockView(mContext);
                mViewHolder.mDigitalClockView.setDigitalClock(mClock);
                mContext.sendBroadcast(new Intent(Clock.COM_INGENIC_READY_ACTION));
                contentView = mViewHolder.mDigitalClockView;
                break;
            case CLOCK_ANALOG_TYPE:
                // 模拟表盘
                mViewHolder.mAnalogClockView = new AnalogClockView(mContext);
                contentView = mViewHolder.mAnalogClockView;
                mViewHolder.mAnalogClockView.setAnalogClock(mClock);
                break;
            default:
                break;
            }
            contentView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) contentView.getTag();
            switch (clockType) {
            case CLOCK_DIGITAL_TYPE:
                mViewHolder.mDigitalClockView.setDigitalClock(mClock);
                break;
            case CLOCK_ANALOG_TYPE:
                mViewHolder.mAnalogClockView.setAnalogClock(mClock);
                break;
            default:
                break;
            }
        }
        return contentView;
    }

    public class ViewHolder {
        private DigitalClockView mDigitalClockView;
        private AnalogClockView mAnalogClockView;
    }

}
