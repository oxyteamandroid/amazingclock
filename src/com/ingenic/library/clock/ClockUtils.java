/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.clock;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Xml;

/**
 * 表盘工具类
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class ClockUtils {
    public static final String DESC_FILE = "clock_desc.xml";

    public static final String ns = null;

    public static Clock getClockFromPath(String mClockPath) {
        Clock clock = new Clock();
        try {
            FileInputStream in = new FileInputStream(new File(mClockPath
                    + File.separator + DESC_FILE));
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, null, "clock");
            while (parser.nextTag() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();

                if (name.equals("clock_id")) {
                    clock.setmClockId(readTagValues(parser, "clock_id"));
                } else if (name.equals("type")) {
                    clock.setmType(readTagValues(parser, "type"));
                } else if (name.equals("moon_name")) {
                    clock.setmMoonName(readTagValues(parser, "moon_name"));
                } else if (name.equals("moon_view_bg")) {
                    clock.setmMoonView(readTagValues(parser, "moon_view_bg"));
                } else if (name.equals("sunrise_note")) {
                    clock.setmSunriseNote(readTagValues(parser, "sunrise_note"));
                } else if (name.equals("sunrise_bg")) {
                    clock.setmSunriseBg(readTagValues(parser, "sunrise_bg"));
                } else if (name.equals("sunset_note")) {
                    clock.setmSunsetNote(readTagValues(parser, "sunset_note"));
                } else if (name.equals("sunset_bg")) {
                    clock.setmSunsetBg(readTagValues(parser, "sunset_bg"));
                } else if (name.equals("weather_note")) {
                    clock.setmWeatherNote(readTagValues(parser, "weather_note"));
                } else if (name.equals("preclock")) {
                    clock.setmPreclock(readTagValues(parser, "preclock"));
                } else if (name.equals("style")) {
                    clock.setmStyle(readTagValues(parser, "style"));
                } else if (name.equals("clock_name")) {
                    clock.setmClockNameValues(readTagValues(parser,
                            "clock_name"));
                } else if (name.equals("timecolor")) {
                    clock.setTimeColor(readTagValues(parser, "timecolor"));
                } else if (name.equals("background")) {
                    clock.setmBackground(readTagValues(parser, "background"));
                } else if (name.equals("dislunar")) {
                    clock.setDisLunar(readTagValues(parser, "dislunar"));
                } else if (name.equals("lunarLocation")) {
                    clock.setLunarLocation(readTagValues(parser,
                            "lunarLocation"));
                } else if (name.equals("hourLocation")) {
                    clock.setHourLocation(readTagValues(parser, "hourLocation"));
                } else if (name.equals("timeSeLocation")) {
                    clock.setTimeSelocation(readTagValues(parser,
                            "timeSeLocation"));
                } else if (name.equals("minuteLocation")) {
                    clock.setMinuteLocation(readTagValues(parser,
                            "minuteLocation"));
                } else if (name.equals("monthdayweekLocation")) {
                    clock.setMonthDayWeekLocation(readTagValues(parser,
                            "monthdayweekLocation"));
                } else if (name.equals("timecolor")) {
                    clock.setmTimeColor(readTagValues(parser, "timecolor"));
                } else if (name.equals("hour_point")) {
                    // analog clock
                    clock.setmHourPoint(readTagValues(parser, "hour_point"));
                } else if (name.equals("minute_point")) {
                    // analog clock
                    clock.setmMinutePoint(readTagValues(parser, "minute_point"));
                } else if (name.equals("second_progress")) {
                    // analog clock
                    clock.setmSecondProgress(readTagValues(parser,
                            "second_progress"));
                } else if (name.equals("second_point")) {
                    // analog clock
                    clock.setmSecondPoint(readTagValues(parser, "second_point"));
                } else if (name.equals("month_bg")) {
                    // analog clock
                    clock.setmMonthBg(readTagValues(parser, "month_bg"));
                } else if (name.equals("month_point")) {
                    // analog clock
                    clock.setmMonthPoint(readTagValues(parser, "month_point"));
                } else if (name.equals("week_bg")) {
                    // analog clock
                    clock.setmWeekBg(readTagValues(parser, "week_bg"));
                } else if (name.equals("week_point")) {
                    // analog clock
                    clock.setmWeekPoint(readTagValues(parser, "week_point"));
                } else if (name.equals("ampm_bg")) {
                    // analog clock
                    clock.setmAmpmBg(readTagValues(parser, "ampm_bg"));
                } else if (name.equals("ampm_point")) {
                    // analog clock
                    clock.setmAmpmPoint(readTagValues(parser, "ampm_point"));
                } else if (name.equals("daynotelocation")) {
                    // analog clock
                    clock.setmDayNoteLocation(readTagValues(parser,
                            "daynotelocation"));
                } else if (name.equals("ampmnotelocation")) {
                    // analog clock
                    clock.setmAmpmNotelocation(readTagValues(parser,
                            "ampmnotelocation"));
                } else if (name.equals("weeknotelocation")) {
                    // analog clock
                    clock.setmWeekNotelocation(readTagValues(parser,
                            "weeknotelocation"));
                } else {
                    skip(parser);
                }
            }

            if (in != null) {
                in.close();
            }

        } catch (FileNotFoundException e) {
            // e.printStackTrace();
        } catch (XmlPullParserException e) {
            // e.printStackTrace();
        } catch (IOException e) {
            // e.printStackTrace();
        }

        clock.setmFilePath(mClockPath);
        return clock;
    }

    /**
     * 获取表盘的名称
     *
     * @param clockName
     * @param context
     * @return
     */
    public static String getLocalClockName(String[] clockName, Context context) {
        if (clockName.length > ClockProperty.CLOCK_DISPALY_NAME_ENGLISH) {
            String language = Locale.getDefault().toString();

            if (language.equals("zh_CN")) {
                return clockName[ClockProperty.CLOCK_DISPALY_NAME_CHINESE];
            } else if (language.equals("zh_TW")) {
                return clockName[ClockProperty.CLOCK_DISPALY_NAME_TRAN_CHINESE];
            } else {
                return clockName[ClockProperty.CLOCK_DISPALY_NAME_ENGLISH];
            }
        } else {
            return context.getString(R.string.clock_name);
        }
    }

    public static int getMoonIndex(int moonLevel) {
        if (moonLevel == 30 || moonLevel == 1) {
            return 0;
        } else if (moonLevel >= 2 && moonLevel <= 6) {
            return 1;
        } else if (moonLevel >= 7 && moonLevel <= 10) {
            return 2;
        } else if (moonLevel >= 11 && moonLevel <= 14) {
            return 3;
        } else if (moonLevel >= 15 && moonLevel <= 16) {
            return 4;
        } else if (moonLevel >= 17 && moonLevel <= 21) {
            return 5;
        } else if (moonLevel >= 22 && moonLevel <= 24) {
            return 6;
        } else if (moonLevel >= 25 && moonLevel <= 26) {
            return 7;
        } else if (moonLevel >= 27 && moonLevel <= 29) {
            return 8;
        } else {
            return 0;
        }
    }

    /**
     * 
     * @param chinaDay
     * @return
     */
    public static String getMoonName(int chinaDay, Context context) {
        int moonNameId = -1;
        switch (chinaDay) {
        case ClockProperty.CHINA_LUNAR_DAY_0:
            moonNameId = R.string.moon0;
            break;
        case ClockProperty.CHINA_LUNAR_DAY_1:
            moonNameId = R.string.moon1;
            break;
        case ClockProperty.CHINA_LUNAR_DAY_2:
            moonNameId = R.string.moon2;
            break;
        case ClockProperty.CHINA_LUNAR_DAY_3:
            moonNameId = R.string.moon3;
            break;
        case ClockProperty.CHINA_LUNAR_DAY_4:
            moonNameId = R.string.moon4;
            break;
        case ClockProperty.CHINA_LUNAR_DAY_5:
            moonNameId = R.string.moon3;
            break;
        case ClockProperty.CHINA_LUNAR_DAY_6:
            moonNameId = R.string.moon5;
            break;
        case ClockProperty.CHINA_LUNAR_DAY_7:
            moonNameId = R.string.moon1;
            break;
        case ClockProperty.CHINA_LUNAR_DAY_8:
            moonNameId = R.string.moon6;
            break;
        default:
            break;
        }
        if (moonNameId == -1)
            return null;
        return context.getResources().getString(moonNameId);
    }

    public static String formatTime(int time) {
        if (time < 10) {
            return 0 + "" + time;
        }
        return time + "";
    }

    public static String getSunRiseSetTimeString(int time) {
        return formatTime(time / 60) + ":" + formatTime(time % 60);
    }

    /**
     * 获得月相的背景
     * 
     * @param chinaDay
     * @return
     */

    public static Bitmap getMoonIndxeBitmap(int chinaDay, Clock clock) {

        String moonName = null;
        Bitmap mBitmap = null;
        switch (chinaDay) {
        case ClockProperty.CHINA_LUNAR_DAY_0:
            moonName = "moon0";
            break;
        case ClockProperty.CHINA_LUNAR_DAY_1:
            moonName = "moon1";
            break;
        case ClockProperty.CHINA_LUNAR_DAY_2:
            moonName = "moon2";
            break;
        case ClockProperty.CHINA_LUNAR_DAY_3:
            moonName = "moon3";
            break;
        case ClockProperty.CHINA_LUNAR_DAY_4:
            moonName = "moon4";
            break;
        case ClockProperty.CHINA_LUNAR_DAY_5:
            moonName = "moon5";
            break;
        case ClockProperty.CHINA_LUNAR_DAY_6:
            moonName = "moon6";
            break;
        case ClockProperty.CHINA_LUNAR_DAY_7:
        case ClockProperty.CHINA_LUNAR_DAY_8:
            moonName = "moon7";
            break;
        default:
            break;
        }
        File moonFile = new File(clock.getmFilePath() + File.separator + moonName + ".png");
        try {
            if (moonFile.exists()) {
                FileInputStream inputHour = new FileInputStream(moonFile);
                mBitmap = BitmapFactory.decodeStream(new BufferedInputStream(
                        inputHour));
                inputHour.close();
                return mBitmap;
            } else {
                return null;
            }
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }

    }

    public static Bitmap getSunRiseSetBg(Clock clock, String sunRiseSetName) {
        File sunFile = new File(clock.getmFilePath() + File.separator + sunRiseSetName);
        Bitmap mBitmap = null;
        try {
            if (sunFile.exists()) {
                FileInputStream inputHour = new FileInputStream(sunFile);
                mBitmap = BitmapFactory.decodeStream(new BufferedInputStream(
                        inputHour));
                inputHour.close();
                return mBitmap;
            } else {
                return null;
            }
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * 格式化用，使得输入的为0-9的值变为00-09,大于9的则不变
     *
     * @param value
     * @return
     */
    public static String formatClockTime(int value) {
        String tmpStr = String.valueOf(value);
        if (value < 10) {
            tmpStr = "0" + tmpStr;
        }
        return tmpStr;
    }

    /**
     * 返回指针的样式
     *
     * @param pointProgressType
     * @return
     */
    public static int getPointProgressType(String pointProgressType) {
        if (pointProgressType.equals(ClockProperty.POINT_LINK_STROKE)) {
            return ClockProperty.LINK_STROKE;
        } else if (pointProgressType.equals(ClockProperty.POINT_SPACE_SMALL)) {
            return ClockProperty.SPACE_SMALL;
        } else if (pointProgressType.equals(ClockProperty.POINT_SPACE_BIG)) {
            return ClockProperty.SPACE_BIG;
        }
        return ClockProperty.LINK_STROKE;
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatTime(String millionSecond) {

        try {

            if (millionSecond.substring(millionSecond.length() - 5,
                    millionSecond.length()).equals("apple")) {
                String timeTemp = millionSecond.substring(0, 4) + "-"
                        + millionSecond.substring(4, 6) + "-"
                        + millionSecond.substring(6, 8) + "-"
                        + millionSecond.substring(9, 11) + ":" +

                        millionSecond.substring(11, 13) + ":"
                        + millionSecond.substring(13, 15);
                return timeTemp;
            } else {

                long currentTime = Long.parseLong(millionSecond);

                SimpleDateFormat formatter = new SimpleDateFormat(
                        "yyyy-MM-dd-HH:mm:ss");

                Date date = new Date(currentTime);
                String strDate = formatter.format(date);
                return strDate;

            }
        } catch (Exception e) {
            return "";
        }

    }

    public static String readTagValues(XmlPullParser parser, String tagStr)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tagStr);
        String tagValues = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tagStr);
        return tagValues;
    }

    public static String readText(XmlPullParser parser) throws IOException,
            XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    public static void skip(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                depth--;
                break;
            case XmlPullParser.START_TAG:
                depth++;
                break;
            }
        }
    }

}
