/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.clock;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.format.Time;
import android.util.Log;
import android.view.View;

import com.ingenic.library.analog.AnalogClockView;

/**
 * 显示指针view
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class PointSecondsView extends View {
    public static final boolean DEBUG = false;
    protected Time mCalendar;

    protected Drawable mSecondHand = null;

    private Bitmap mBitmap = null;
    private float mSecond;

    protected int mDialWidth;
    protected int mDialHeight;
    protected boolean mChanged;
    private Clock mClock;

    private boolean SecondisEnd = true;

    public PointSecondsView(Context context) {
        super(context);
        setWillNotDraw(false);
    }

    public PointSecondsView(Context context, Clock clock) {
        this(context);
        mClock = clock;
        recycleBitmap();
        mCalendar = new Time();
        String clockPath = mClock.getmFilePath();
        setFiles(new File(clockPath + "/" + mClock.getmHourPoint()), new File(
                clockPath + "/" + mClock.getmMinutePoint()), new File(clockPath
                + "/" + mClock.getmSecondPoint()));
    }

    private String mTimeZoneId;

    public void startClockThread() {
        SecondisEnd = false;
        getCurrentTime();

        String[] preClock = null;
        if (mClock.getmPreclock() != null) {
            preClock = mClock.getmPreclock().split(",");
        }
        float animationHour = -1;
        float animationMinute = -1;
        float animationSecond = -1;

        if (preClock != null && preClock.length > ClockProperty.PRE_CLOCK_HOUR) {
            animationHour = Integer
                    .parseInt(preClock[ClockProperty.PRE_CLOCK_HOUR]);
        }

        if (preClock != null
                && preClock.length > ClockProperty.PRE_CLOCK_MINUTE) {
            animationMinute = Integer
                    .parseInt(preClock[ClockProperty.PRE_CLOCK_MINUTE]);
        }

        if (preClock != null
                && preClock.length > ClockProperty.PRE_CLOCK_SECOND) {
            animationSecond = Integer
                    .parseInt(preClock[ClockProperty.PRE_CLOCK_SECOND]);
        }

        animationHour = animationHour + animationMinute / 60;

        if (mSecondHand != null) {
            mSecond = mSecond + 1;
            if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {
                startSecondAnimation(mSecond, animationSecond);
               // mSecond = animationSecond;
            } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_NORMAL) {
                startSecondAnimation(animationSecond, mSecond);
            }
           // SecondisEnd = true;
           // invalidate();
        } else {
            SecondisEnd = true;
        }
    }

    public void updateTime(String id) {
        if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {
            startClockThread();
        } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_NORMAL) {
            startClockThread();
        } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_TIME_NORMAL) {
            if (SecondisEnd) {
                mTimeZoneId = id;
                onTimeChanged();
                invalidate();
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    public void recycleBitmap() {
        if (mBitmap != null && !mBitmap.isRecycled()) {
            mBitmap.recycle();
            mBitmap = null;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    private void getCurrentTime() {
        mCalendar.setToNow();
        if (mTimeZoneId != null) {
            mCalendar.switchTimezone(mTimeZoneId);
        }
        float second = mCalendar.second;
        mSecond = second;
    }

    private void onTimeChanged() {
        float oldSecond = mSecond;
        getCurrentTime();
        mChanged = oldSecond != mSecond;
    }

    @SuppressWarnings("deprecation")
    public void setFiles(File hourFile, File minuteFile, File secondFile) {
        try {

            if (secondFile.exists()) {
                FileInputStream inputSecond = new FileInputStream(secondFile);
                mBitmap = BitmapFactory.decodeStream(new BufferedInputStream(
                        inputSecond));
                mSecondHand = new BitmapDrawable(mBitmap);
                inputSecond.close();
            } else {
                mSecondHand = null;
            }
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    private void startSecondAnimation(float startPosition, float stopPosition) {

        ValueAnimator mValue = ValueAnimator.ofFloat(startPosition,
                stopPosition);
        mValue.setDuration(ClockProperty.POINT_SCALE_INTERVAL_TIME);
        mValue.addUpdateListener(new AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // TODO Auto-generated method stub
                float valueInt = ((Float) animation.getAnimatedValue())
                        .intValue();
                mSecond = valueInt;
                invalidate();

                Log.e("test", "===test==valueInt===" + valueInt);

            }
        });

        mValue.addListener(new AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // TODO Auto-generated method stub
                SecondisEnd = true;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // TODO Auto-generated method stub
            }
        });

        mValue.start();

    }

    protected void onDraw(Canvas canvas) {
        // getCurrentTime();
        int availableWidth = getRight() - getLeft();
        int availableHeight = getBottom() - getTop();

        int x = availableWidth / 2;
        int y = availableHeight / 2;
        int w, h;

        // draw second hand
        if (mSecondHand != null) {
            canvas.save();
            canvas.rotate((mSecond / 60.0f) * 360.0f, x, y);
            final Drawable secondHand = mSecondHand;
            w = (int) ((secondHand.getIntrinsicWidth()) * 1.5);
            h = (int) ((secondHand.getIntrinsicHeight()) * 1.5);
            secondHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y
                    + (h / 2));
            secondHand.draw(canvas);
            canvas.restore();
        }
    }
}
