/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.library.clock;

/**
 * 常量
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class ClockProperty {

    /**
     * 月份背景图片
     */
    public static final int MONTH_BG_VALUE = 0;

    /**
     * 月份显示的 x 坐标
     */
    public static final int MONTH_X_VALUE = 1;

    /**
     * 月份显示的 y 坐标
     */
    public static final int MONTH_Y_VALUE = 2;

    /**
     * 月份是否需要显示
     */
    public static final int MONTH_DISPLAY = 3;

    /**
     * 月份显示的宽度
     */
    public static final int MONTH_WIDTH = 4;

    /**
     * 月份显示的高度
     */
    public static final int MONTH_HEIGHT = 5;

    /**
     * 显示周背景
     */
    public static final int WEEK_BG_VALUE = 0;

    /**
     * 周显示 x 坐标
     */
    public static final int WEEK_BG__X_VALUE = 1;

    /**
     * 周显示 y 坐标
     */
    public static final int WEEK_BG__Y_VALUE = 2;

    /**
     * 周显示字体颜色
     */
    public static final int WEEK_BG_COLOR = 3;

    /**
     * 周显示字体
     */
    public static final int WEEK_BG_FONT = 4;

    /**
     * 周是否显示
     */
    public static final int WEEK_BG_DISPLAY = 5;

    /**
     * 周显示宽度
     */
    public static final int WEEK_BG_WIDTH = 6;

    /**
     * 周显示高度
     */
    public static final int WEEK_BG_HEIGHT = 7;

    /**
     * 上午、下午背景
     */
    public static final int AM_PM_BG_VALUE = 0;

    /**
     * 上午、下午 x 坐标
     */
    public static final int AM_PM_X_VALUE = 1;

    /**
     * 上午、下午 y 坐标
     */
    public static final int AM_PM_Y_VALUE = 2;

    /**
     * 上午、下午背景颜色
     */
    public static final int AM_PM_COLOR = 3;

    /**
     * 上午、下午背景字体
     */
    public static final int AM_PM_FONT = 4;

    /**
     * 上午、下午背景是否显示
     */
    public static final int AM_PM_DISPLAY = 5;

    /**
     * 上午、下午背景宽度
     */
    public static final int AM_PM_WIDTH = 6;

    /**
     * 上午、下午背景高度
     */
    public static final int AM_PM_HEIGHT = 7;

    /**
     * 上午、下午字的 x 坐标
     */
    public static final int AM_PM_NOTE_X_VALUE = 0;

    /**
     * 上午、下午字的 y 坐标
     */
    public static final int AM_PM_NOTE_Y_VALUE = 1;

    /**
     * 上午、下午字的字体大小
     */
    public static final int AM_PM_NOTE_FONT_SIZE = 2;

    /**
     * 上午、下午字的字体颜色
     */
    public static final int AM_PM_NOTE_FONT_COLOR = 3;

    /**
     * 上午、下午字的字体
     */
    public static final int AM_PM_NOTE_FONT = 4;

    /**
     * 上午、下午字是否显示
     */
    public static final int AM_PM_NOTE_DISPLAY = 5;

    /**
     * 日期的 x 坐标
     */
    public static final int DAY_NOTE_X_VALUE = 0;

    /**
     * 日期的 y 坐标
     */
    public static final int DAY_NOTE_Y_VALUE = 1;

    /**
     * 日期的字体大小
     */
    public static final int DAY_NOTE_FONT_SIZE = 2;

    /**
     * 日期的字体颜色
     */
    public static final int DAY_NOTE_FONT_COLOR = 3;

    /**
     * 日期的字体
     */
    public static final int DAY_NOTE_FONT = 4;

    /**
     * 日期的背景
     */
    public static final int DAY_NOTE_BG = 5;

    /**
     * 日期是否显示
     */
    public static final int DAY_NOTE_DISPLAY = 6;

    /**
     * 显示日期的格式
     */
    public static final int DAY_NOTE_TYPE = 7;

    /**
     * 显示表盘名称,中文
     */
    public static final int CLOCK_DISPALY_NAME_CHINESE = 0;

    /**
     * 显示表盘名称,繁体
     */
    public static final int CLOCK_DISPALY_NAME_TRAN_CHINESE = 1;

    /**
     * 显示表盘名称,英文
     */
    public static final int CLOCK_DISPALY_NAME_ENGLISH = 2;

    /**
     * 显示表盘名称,字体大小
     */
    public static final int CLOCK_DISPALY_NAME_SIZE = 3;

    /**
     * 显示表盘名称,字体颜色
     */
    public static final int CLOCK_DISPALY_NAME_COLOR = 4;

    /**
     * 显示日期的格式,DD_MM ，表示显示月和日
     */
    public static final String DAY_TEYP_DAY_MONTH = "MM_DD";

    /**
     * 周的 x 坐标
     */
    public static final int WEEK_NOTE_X_VALUE = 0;

    /**
     * 周的 y 坐标
     */
    public static final int WEEK_NOTE_Y_VALUE = 1;

    /**
     * 周的字体大小
     */
    public static final int WEEK_NOTE_FONT_SIZE = 2;

    /**
     * 周的字体颜色
     */
    public static final int WEEK_NOTE_FONT_COLOR = 3;

    /**
     * 周的字体
     */
    public static final int WEEK_NOTE_FONT = 4;

    /**
     * 周是否显示
     */
    public static final int WEEK_NOTE_DISPLAY = 5;

    /**
     * 农历显示 x 坐标
     */
    public static final int LUNAR_X_VALUE = 0;

    /**
     * 农历显示 y 坐标
     */
    public static final int LUNAR_Y_VALUE = 1;

    /**
     * 农历显示字体大小
     */
    public static final int LUNAR_FONT_SIZE = 2;

    /**
     * 农历显示字体颜色
     */
    public static final int LUNAR_FONT_COLOR = 3;

    /**
     * 农历显示字体
     */
    public static final int LUNAR_FONT = 4;

    /**
     * 时间提示 x 坐标
     */
    public static final int TIME_NOTE_X_VALUE = 0;

    /**
     * 时间提示 y 坐标
     */
    public static final int TIME_NOTE_Y_VALUE = 1;

    /**
     * 时间提示字体大小
     */
    public static final int TIME_NOTE_FONT_SIZE = 2;

    /**
     * 时间提示字体颜色
     */
    public static final int TIME_NOTE_FONT_COLOR = 3;

    /**
     * 时间提示字体
     */
    public static final int TIME_NOTE_FONT = 4;

    /**
     * 小时提示 x 坐标
     */
    public static final int HOUR_X_VALUE = 0;

    /**
     * 小时提示 y 坐标
     */
    public static final int HOUR_Y_VALUE = 1;

    /**
     * 小时提示字体大小
     */
    public static final int HOUR_FONT_SIZE = 2;

    /**
     * 小时提示字体颜色
     */
    public static final int HOUR_FONT_COLOR = 3;

    /**
     * 小时提示字体
     */
    public static final int HOUR_FONT = 4;

    /**
     * 小时，是否显示
     */
    public static final int HOUR_DISPLAY = 5;

    /**
     * 小时，显示的背景
     */
    public static final int HOUR_DISPLAY_BG = 6;

    /**
     * 分提示 x 坐标
     */
    public static final int MINUTE_X_VALUE = 0;

    /**
     * 分提示 y 坐标
     */
    public static final int MINUTE_Y_VALUE = 1;

    /**
     * 分提示字体大小
     */
    public static final int MINUTE_FONT_SIZE = 2;

    /**
     * 分提示字体颜色
     */
    public static final int MINUTE_FONT_COLOR = 3;

    /**
     * 分提示字体
     */
    public static final int MINUTE_FONT = 4;

    /**
     * 分，是否显示
     */
    public static final int MINUTE_DISPLAY = 5;

    /**
     * 分，显示的背景
     */
    public static final int MINUTE_DISPLAY_BG = 6;

    /**
     * 周月提示 x 坐标
     */
    public static final int MONTH_WEEK_X_VALUE = 0;

    /**
     * 周月提示 y 坐标
     */
    public static final int MONTH_WEEK_Y_VALUE = 1;

    /**
     * 周月提示字体大小
     */
    public static final int MONTH_WEEK_FONT_SIZE = 2;

    /**
     * 周月提示字体颜色
     */
    public static final int MONTH_WEEK_FONT_COLOR = 3;

    /**
     * 周月提示字体
     */
    public static final int MONTH_WEEK_FONT = 4;

    /**
     * 预览表盘 小时
     */
    public static final int PRE_CLOCK_HOUR = 0;

    /**
     * 预览表盘 分钟
     */
    public static final int PRE_CLOCK_MINUTE = 1;

    /**
     * 预览表盘 秒
     */
    public static final int PRE_CLOCK_SECOND = 2;

    /**
     * 表盘缩小
     */
    public static final int CLOCK_SCALE_SMALL = 0;

    /**
     * 表盘正常
     */
    public static final int CLOCK_SCALE_NORMAL = 1;

    /**
     * 表盘 时间正常显示
     */
    public static final int CLOCK_TIME_NORMAL = 2;

    /**
     * 表盘从正常到缩放 之间的时间间隔，表盘从缩放到正常 之间的时间间隔 毫秒
     */
    public static final int ANIMATION_SCALE_DURATION = 500;

    /**
     * 表盘从正常到缩小，时间上面从正常回到默认设置的时间的动画，过渡的时间的间隔 毫秒
     */
    public static final int POINT_SCALE_INTERVAL_TIME = 50;

    /**
     * 指针每走一次，需要多少毫秒的时间
     */
    public static final int POINT_INTERVAL_ONE_TIME = 50;

    /**
     * 秒刷新action
     */
    public static final String SECOND_ACTION_UPDATE = "com.ingenic.second.update";

    /**
     * 指针 progress 类型
     */
    public static final int POINT_PROGRESS_TYPE = 0;

    /**
     * 指针 progress 起始颜色
     */
    public static final int POINT_PROGRESS_START_COLOR = 1;

    /**
     * 指针 progress 结束颜色
     */
    public static final int POINT_PROGRESS_END_COLOR = 2;

    /**
     * 指针 线性的样式
     */
    public static final int LINK_STROKE = 0;

    /**
     * 指针 实心 样式
     */
    public static final int LINK_FILL = 1;

    /**
     * 指针 间距大的样式
     */
    public static final int SPACE_BIG = 2;

    /**
     * 指针，间距小的样式
     */
    public static final int SPACE_SMALL = 3;

    /**
     * 指针的样式 link_stroke
     */
    public static final String POINT_LINK_STROKE = "link_stroke";

    /**
     * 指针的样式 space_small
     */
    public static final String POINT_SPACE_SMALL = "space_small";

    /**
     * 指针的样式 space_big
     */
    public static final String POINT_SPACE_BIG = "space_big";

    /**
     * 月相名称的x坐标
     */
    public static final int MOON_NAME_NOTE_X = 0;

    /**
     * 月相名称的y坐标
     */
    public static final int MOON_NAME_NOTE_Y = 1;

    /**
     * 月相名称的字体大小
     */
    public static final int MOON_NAME_NOTE_SIZE = 2;

    /**
     * 月相名称的颜色
     */
    public static final int MOON_NAME_NOTE_COLOR = 3;

    /**
     * 月相名称的字体
     */
    public static final int MOON_NAME_NOTE_FONT = 4;

    /**
     * 月相名称，是否显示
     */
    public static final int MOON_NAME_NOTE_DISPLAY = 5;

    /**
     * 月相的背景坐标x
     */
    public static final int MOON_VIEW_BG_X = 0;

    /**
     * 月相的背景坐标y
     */
    public static final int MOON_VIEW_BG_Y = 1;

    /**
     * 月相的背景 新月(朔)
     */
    public static final int MOON_VIEW_BG_NEW = 2;

    /**
     * 月相的背景 峨嵋月
     */
    public static final int MOON_VIEW_BG_CRESCENT = 3;

    /**
     * 月相的背景 上弦月
     */
    public static final int MOON_VIEW_BG_FIRST_QUARTER = 4;

    /**
     * 月相的背景 凸月
     */
    public static final int MOON_VIEW_BG_GIBBOUS = 5;

    /**
     * 月相的背景 满月(望)
     */
    public static final int MOON_VIEW_BG_FULL = 6;

    /**
     * 月相的背景 下弦月
     */
    public static final int MOON_VIEW_BG_LAST_QUARTER = 7;

    /**
     * 月相的背景 残月
     */
    public static final int MOON_VIEW_BG_WANING = 8;

    /**
     * 月相的背景 是否显示
     */
    public static final int MOON_VIEW_BG_DISPLAY = 9;

    /**
     * 日出的提示 x坐标
     */
    public static final int SUN_RISE_NOTE_X = 0;

    /**
     * 日出的提示 y坐标
     */
    public static final int SUN_RISE_NOTE_Y = 1;

    /**
     * 日出的提示 字体大小
     */
    public static final int SUN_RISE_NOTE_SIZE = 2;

    /**
     * 日出的提示 字体的颜色
     */
    public static final int SUN_RISE_NOTE_COLOR = 3;

    /**
     * 日出的提示 字体
     */
    public static final int SUN_RISE_NOTE_FONT = 4;

    /**
     * 日出的提示 是否显示
     */
    public static final int SUN_RISE_NOTE_DISPLAY = 5;

    /**
     * 日出的背景的 x 坐标
     */
    public static final int SUN_RISE_BG_X = 0;

    /**
     * 日出的背景的 y 坐标
     */
    public static final int SUN_RISE_BG_Y = 1;

    /**
     * 日出的背景 资源
     */
    public static final int SUN_RISE_BG_SOURCE = 2;

    /**
     * 日出的背景 是否显示
     */
    public static final int SUN_RISE_BG_DISPLAY = 3;

    /**
     * 日落的提示 x坐标
     */
    public static final int SUN_RET_NOTE_X = 0;

    /**
     * 日落的提示 y坐标
     */
    public static final int SUN_RET_NOTE_Y = 1;

    /**
     * 日落的提示 字体大小
     */
    public static final int SUN_SET_NOTE_SIZE = 2;

    /**
     * 日落的提示 字体的颜色
     */
    public static final int SUN_SET_NOTE_COLOR = 3;

    /**
     * 日落的提示 字体
     */
    public static final int SUN_SET_NOTE_FONT = 4;

    /**
     * 日落的提示 是否显示
     */
    public static final int SUN_SET_NOTE_DISPLAY = 5;

    /**
     * 日落的背景的 x 坐标
     */
    public static final int SUN_SET_BG_X = 0;

    /**
     * 日落的背景的 y 坐标
     */
    public static final int SUN_SET_BG_Y = 1;

    /**
     * 日落的背景 资源
     */
    public static final int SUN_SET_BG_SOURCE = 2;

    /**
     * 日落的背景 是否显示
     */
    public static final int SUN_SET_BG_DISPLAY = 3;

    /**
     * 天气的提示 x 坐标
     */
    public static final int WEATHER_NOTE_X = 0;

    /**
     * 天气的提示 y 坐标
     */
    public static final int WEATHER_NOTE_Y = 1;

    /**
     * 天气的提示 字体的大小
     */
    public static final int WEATHER_NOTE_SIZE = 2;

    /**
     * 天气的提示 字体的颜色
     */
    public static final int WEATHER_NOTE_COLOR = 3;

    /**
     * 天气的提示 字体
     */
    public static final int WEATHER_NOTE_FONT = 4;

    /**
     * 天气的提示 是否显示
     */
    public static final int WEATHER_NOTE_DISPLAY = 5;

    /**
     * 农历的日期
     */
    public static final int CHINA_LUNAR_DAY_0 = 0;
    public static final int CHINA_LUNAR_DAY_1 = 1;
    public static final int CHINA_LUNAR_DAY_2 = 2;
    public static final int CHINA_LUNAR_DAY_3 = 3;
    public static final int CHINA_LUNAR_DAY_4 = 4;
    public static final int CHINA_LUNAR_DAY_5 = 5;
    public static final int CHINA_LUNAR_DAY_6 = 6;
    public static final int CHINA_LUNAR_DAY_7 = 7;
    public static final int CHINA_LUNAR_DAY_8 = 8;

    /**
     * 日出，日落的Preferences
     */
    public static final String SUN_RISE_SET_FLAG = "sunRiseSetPreferences";

    /**
     * 日出的flag
     */
    public static final String SUN_RISE_FLAG = "sun_rise_flag";

    /**
     * 日落的flag
     */
    public static final String SUN_SET_FLAG = "sun_set_flag";

}
