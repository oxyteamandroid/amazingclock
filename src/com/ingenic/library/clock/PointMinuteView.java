/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.clock;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.format.Time;
import android.util.Log;
import android.view.View;

import com.ingenic.library.analog.AnalogClockView;

/**
 * 显示指针view
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class PointMinuteView extends View {
    public static final boolean DEBUG = false;
    protected Time mCalendar;

    protected Drawable mMinuteHand = null;

    private Bitmap mBitmap = null;
    private float mSecond;
    private float mMinutes;

    protected int mDialWidth;
    protected int mDialHeight;
    protected boolean mChanged;
    private Clock mClock;

    private boolean MinuteisEnd = true;

    public PointMinuteView(Context context) {
        super(context);
        setWillNotDraw(false);
    }

    public PointMinuteView(Context context, Clock clock) {
        this(context);
        mClock = clock;
        recycleBitmap();
        mCalendar = new Time();
        String clockPath = mClock.getmFilePath();
        setFiles(new File(clockPath + "/" + mClock.getmHourPoint()), new File(
                clockPath + "/" + mClock.getmMinutePoint()), new File(clockPath
                + "/" + mClock.getmSecondPoint()));
    }

    private String mTimeZoneId;

    public void startClockThread() {
        MinuteisEnd = false;
        getCurrentTime();

        if (mClock.getmPreclock() != null) {
            mClock.getmPreclock().split(",");
        }
        float animationMinute = -1;

        if (mMinuteHand != null) {

            if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {
                startMinuteAnimation(mMinutes, animationMinute);
               // mMinutes = animationMinute;
            } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_NORMAL) {
                startMinuteAnimation(animationMinute, mMinutes);
            }
           // MinuteisEnd = true;
          //  invalidate();

        } else {
            MinuteisEnd = true;
        }

    }

    public void updateTime(String id) {
        if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {
            startClockThread();
        } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_NORMAL) {
            startClockThread();
        } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_TIME_NORMAL) {
            if (MinuteisEnd) {
                mTimeZoneId = id;
                onTimeChanged();
                invalidate();
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    public void recycleBitmap() {
        if (mBitmap != null && !mBitmap.isRecycled()) {
            mBitmap.recycle();
            mBitmap = null;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    private void getCurrentTime() {
        mCalendar.setToNow();
        if (mTimeZoneId != null) {
            mCalendar.switchTimezone(mTimeZoneId);
        }
        float minute = mCalendar.minute;
        float second = mCalendar.second;
        mSecond = second;
        mMinutes = minute;
    }

    private void onTimeChanged() {
        float oldMinutes = mMinutes;
        float oldSecond = mSecond;
        getCurrentTime();
        mChanged = oldMinutes != mMinutes || oldSecond != mSecond;
    }

    @SuppressWarnings("deprecation")
    public void setFiles(File hourFile, File minuteFile, File secondFile) {
        try {

            if (minuteFile.exists()) {
                FileInputStream inputMinute = new FileInputStream(minuteFile);
                mBitmap = BitmapFactory.decodeStream(new BufferedInputStream(
                        inputMinute));
                mMinuteHand = new BitmapDrawable(mBitmap);
                inputMinute.close();
            } else {
                mMinuteHand = null;
            }

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    private void startMinuteAnimation(float startPosition, float stopPosition) {

        ValueAnimator mValue = ValueAnimator.ofFloat(startPosition,
                stopPosition);
        mValue.setDuration(ClockProperty.POINT_SCALE_INTERVAL_TIME);
        mValue.addUpdateListener(new AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // TODO Auto-generated method stub
                float valueInt = ((Float) animation.getAnimatedValue())
                        .intValue();
                mMinutes = valueInt;

                invalidate();

                Log.e("test", "===test==valueInt===" + valueInt);

            }
        });

        mValue.addListener(new AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // TODO Auto-generated method stub
                MinuteisEnd = true;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // TODO Auto-generated method stub
            }
        });

        mValue.start();

    }

    protected void onDraw(Canvas canvas) {
        // getCurrentTime();
        int availableWidth = getRight() - getLeft();
        int availableHeight = getBottom() - getTop();

        int x = availableWidth / 2;
        int y = availableHeight / 2;
        int w, h;

        // draw minute hand
        if (mMinuteHand != null) {
            canvas.save();
            canvas.rotate(((mMinutes / 60.0f) * 360.0f)
                    + ((mSecond / 60) * (360.0f / 60)), x, y);
            final Drawable minuteHand = mMinuteHand;
            w = (int) ((minuteHand.getIntrinsicWidth()) * 1.5);
            h = (int) ((minuteHand.getIntrinsicHeight()) * 1.5);
            minuteHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y
                    + (h / 2));
            minuteHand.draw(canvas);
            canvas.restore();
        }

    }
}
