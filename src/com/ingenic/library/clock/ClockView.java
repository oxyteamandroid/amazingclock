/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.clock;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

/**
 * 表盘
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public abstract class ClockView extends RelativeLayout {

    public View viewRoot;
    public Clock clock;
    public Context mContext;
    private Bitmap mBitmap = null;

    public enum BondState {
        BT_DISABLE, BONDED, UNBOND, BOUND_CALL, UNBOUND_CALL;
    }

    public ClockView(Context context) {
        super(context);
    }

    public void initDefaultView(View view) {
        recycleBitmap();
        viewRoot = view;
        try {
            setBackground();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    public void recycleBitmap() {
        if (mBitmap != null && !mBitmap.isRecycled()) {
            mBitmap.recycle();
            mBitmap = null;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        // TODO Auto-generated method stub
        super.onDetachedFromWindow();
    }

    public void setBackground(Drawable background) {
        if (viewRoot != null) {
            viewRoot.setBackground(background);
        } else {
            super.setBackground(background);
        }
    }

    @SuppressWarnings("deprecation")
    public void setBackground() throws IOException {
        if (clock == null)
            return;
        Drawable mBackground = null;
        if (!"".equals(clock.getmBackground())) {
            FileInputStream input;
            try {
                input = new FileInputStream(new File(clock.getmFilePath()
                        + File.separator + clock.getmBackground()));

                mBitmap = BitmapFactory.decodeStream(new BufferedInputStream(
                        input));
                mBackground = new BitmapDrawable(mBitmap);
                input.close();
                setBackground(mBackground);
            } catch (Exception e) {
            }
        }
    }

    public abstract void updateTime();
}
