/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.clock;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.ingenic.library.analog.AnalogClockView;
import com.ingenic.library.clock.TCAmazingViewPager.OnPageChangeListener;
import com.ingenic.library.digital.DigitalClockView;

/*****
 * 使用clock demo
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class MainActivity extends Activity {

    /*** 装载各种表盘的容器 ***/
    private TCAmazingViewPager mClockViewPager;
    private ClockViewManager mClockViewManager;

    private DigitalClockView mDigitalClockView;
    private AnalogClockView mAnalogClockView;
    private List<Clock> mClockList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        mClockViewManager = ClockViewManager.getInstance(this);
        mClockViewPager = (TCAmazingViewPager) findViewById(R.id.clockViewPager);
        mClockViewPager.setTouchType(TCAmazingViewPager.HORIZONTAL);
        mClockViewPager.setCircularable(true);
        mClockList = new ArrayList<Clock>();
        mClockViewPager.setOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageChange(int old, int current) {
            }
        });
        readClocks();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    private void readClocks() {
        new ClocksTask().execute("load");
    }

    public class ClocksTask extends AsyncTask<String, Void, List<Clock>> {
        @Override
        protected void onPostExecute(List<Clock> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (result != null) {
                mClockList.clear();
                mClockList.addAll(result);
                for (int index = 0; index < mClockList.size(); index++) {
                    if (Clock.TYPE_DIGITAL.equals(mClockList.get(index)
                            .getmType())) {
                        // 数字表盘
                        mDigitalClockView = new DigitalClockView(
                                MainActivity.this);
                        mDigitalClockView
                                .setDigitalClock(mClockList.get(index));
                        mClockViewPager.addView(mDigitalClockView, index);
                    } else if (Clock.TYPE_ANALOG.equals(mClockList.get(index)
                            .getmType())) {
                        // 模拟表盘
                        mAnalogClockView = new AnalogClockView(
                                MainActivity.this);
                        mAnalogClockView.setAnalogClock(mClockList.get(index));
                        mClockViewPager.addView(mAnalogClockView, index);
                    }
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Clock> doInBackground(String... params) {
            if ("load".equals(params[0])) {
                return mClockViewManager.readClocks(true);
            } else {
                return null;
            }
        }
    }
}
