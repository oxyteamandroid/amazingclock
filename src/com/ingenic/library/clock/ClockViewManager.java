/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.clock;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

/**
 * clock 管理类
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class ClockViewManager {

    private static final String CLOCK_PATH = "/system/clocks/";
    private static final String CLOCK_ROUND = "clock_round/";
    private static final String CLOCK_SQUARE = "clock_square/";
    private static ClockViewManager sInstance = null;
    private List<Clock> clocks = null;
    int current = 0;

    public static ClockViewManager getInstance(Context context) {
        if (sInstance == null)
            sInstance = new ClockViewManager(context);
        return sInstance;
    }

    private ClockViewManager(Context context) {
    }

    public List<Clock> readClocks(boolean isround) {
        String clockpath;
        clocks = new ArrayList<Clock>();
        clocks.clear();
        if (isround) {
            clockpath = CLOCK_PATH + CLOCK_ROUND;
        } else {
            clockpath = CLOCK_PATH + CLOCK_SQUARE;
        }
        try {
            String[] files = new File(clockpath).list();
            for (String file : files) {
                Clock clock = ClockUtils.getClockFromPath(clockpath + file);
                clocks.add(clock);
            }
            return clocks;
        } catch (Exception e) {
            // e.printStackTrace();
            return null;
        }
    }

}
