/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.library.clock;

import com.ingenic.library.analog.AnalogClockView;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.format.Time;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * 显示指针view
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class PointHourProgress extends View {

    /**
     * 画笔对象的引用
     */
    private Paint mPaint;
    /**
     * 所画矩形区域
     */
    private RectF mSectionRect;

    /**
     * 高度对宽度的比率
     */
    private float mHeightWidthRatio;

    /**
     * 最外圆的半径对高度的比率
     */
    private float mOuterRadiusHeightRatio;

    /**
     * 目前不用 因为不需要自己画底圆形状
     */
    private int mRoundColor;

    /**
     * 进度条开始的颜色
     */
    private int mRoundProgressStartColor;

    /**
     * 进度条结束的颜色
     */
    private int mRoundProgressFinishColor;

    /**
     * 文字进度颜色
     */
    private int mTextColor;

    /**
     * 文字进度大小
     */
    private int mTextSize;

    /**
     * 最大进度值
     */
    private int mMaxProgress;
    private int mProgress;

    /**
     * 文字进度是否显示
     */
    private boolean mTextIsDisplayable;

    /**
     * 进度条风格：连续空心，连续实心，间隔大，间隔小
     */
    private int mStyle;

    /**
     * 屏幕宽度X轴正中间
     */
    protected float mCenterX;

    /**
     * 屏幕宽度Y轴正中间
     */
    protected float mCenterY;

    /**
     * 圆环进度条半径
     */
    protected float mRadius;

    /**
     * 进度条的高度
     */
    protected float mSectionHeight;

    /**
     * 进度条的宽度
     */
    protected float mSectionWidth;

    /**
     * 进度条风格：连续空心
     */
    private static final int LINK_STROKE = 0;

    /**
     * 进度条风格：连续实心
     */
    private static final int LINK_FILL = 1;

    /**
     * 进度条风格：间隔大
     */
    private static final int SPACE_BIG = 2;

    /**
     * 进度条风格：间隔小
     */
    private static final int SPACE_SMALL = 3;

    /**
     * 表盘的参数
     */
    private Clock mClock;

    /**
     * 时间
     */
    protected Time mCalendar;

    private float mHour;
    private float mMinutes;
    protected boolean mChanged;
    private boolean SecondisEnd = true;

    public PointHourProgress(Context context) {
        super(context);
    }

    public PointHourProgress(Context context, Clock clock) {
        this(context);
        mClock = clock;
        mCalendar = new Time();
    }

    public PointHourProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttribute(context, attrs, 0);
    }

    public PointHourProgress(Context context, AttributeSet attrs,
            int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttribute(context, attrs, defStyleAttr);
    }

    private void initAttribute(Context context, AttributeSet attrs,
            int defStyteAttr) {

        TypedArray typedArray = context.obtainStyledAttributes(attrs,
                R.styleable.RoundProgressBar);
        mRoundColor = typedArray.getColor(
                R.styleable.RoundProgressBar_clockRoundColors, Color.GRAY);
        mRoundProgressStartColor = typedArray.getColor(
                R.styleable.RoundProgressBar_clockRoundProgressStartColors,
                Color.RED);
        mRoundProgressFinishColor = typedArray.getColor(
                R.styleable.RoundProgressBar_clockRoundProgressFinishColors,
                Color.RED);
        mTextColor = typedArray.getColor(
                R.styleable.RoundProgressBar_clockTextColors, Color.RED);
        mTextSize = typedArray.getInt(
                R.styleable.RoundProgressBar_clockTextSizes, 15);
        mMaxProgress = typedArray.getInt(
                R.styleable.RoundProgressBar_clockMaxProgresss, 60);
        mTextIsDisplayable = typedArray.getBoolean(
                R.styleable.RoundProgressBar_clockTextIsDisplayables, true);
        mStyle = typedArray.getInt(R.styleable.RoundProgressBar_clockStyles, 0);
        mOuterRadiusHeightRatio = typedArray.getDimension(
                R.styleable.RoundProgressBar_clockOuterRadiusHeightRatios,
                0.10f);
        mHeightWidthRatio = typedArray.getDimension(
                R.styleable.RoundProgressBar_clockHeightWidthRatios, 5.0f);
        typedArray.recycle();
    }

    private String mTimeZoneId;

    public void updateTime(String id) {
        if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {
            startClockThread();
        } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_NORMAL) {
            startClockThread();
        } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_TIME_NORMAL) {
            if (SecondisEnd) {
                mTimeZoneId = id;
                onTimeChanged();
                if (mChanged) {
                    setProgress((int) mHour);
                }
            }
        }
    }

    private void startClockThread() {
        SecondisEnd = false;
        getCurrentTime();

        String[] preClock = null;
        if (mClock.getmPreclock() != null) {
            preClock = mClock.getmPreclock().split(",");
        }

        float animationSecond = -1;

        if (preClock != null
                && preClock.length > ClockProperty.PRE_CLOCK_SECOND) {
            animationSecond = Integer
                    .parseInt(preClock[ClockProperty.PRE_CLOCK_SECOND]);
        }

        if (mClock.getmSecondProgress() != null) {
            mHour = mHour + 1;
            if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {
                startSecondAnimation(mHour, animationSecond);
                // mSecond = animationSecond;
            } else if (AnalogClockView.mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_NORMAL) {
                startSecondAnimation(animationSecond, mHour);
            }
            // SecondisEnd = true;
            // invalidate();
        } else {
            SecondisEnd = true;
        }
    }

    private void startSecondAnimation(float startPosition, float stopPosition) {

        ValueAnimator mValue = ValueAnimator.ofFloat(startPosition,
                stopPosition);
        mValue.setDuration(ClockProperty.POINT_SCALE_INTERVAL_TIME);
        mValue.addUpdateListener(new AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // TODO Auto-generated method stub
                float valueInt = ((Float) animation.getAnimatedValue())
                        .intValue();
                mHour = valueInt;
                setProgress((int) mHour);

                Log.e("test", "===test==valueInt===" + valueInt);

            }
        });

        mValue.addListener(new AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // TODO Auto-generated method stub
                SecondisEnd = true;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // TODO Auto-generated method stub
            }
        });

        mValue.start();

    }

    private void getCurrentTime() {
        mCalendar.setToNow();
        if (mTimeZoneId != null) {
            mCalendar.switchTimezone(mTimeZoneId);
        }
        float hour = mCalendar.hour;
        float minute = mCalendar.minute;
        mMinutes = minute;
        mHour = hour + mMinutes / 60;
    }

    private void onTimeChanged() {
        float oldHour = mHour;
        getCurrentTime();
        mChanged = oldHour != mHour;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);// 消除锯齿
        float rotation = 360.0f / (float) mMaxProgress;// 得到一个progress偏转角度

        /**
         * 画文字进度百分比
         */
        // 中间的进度百分比，先转换成float在进行除法运算，不然都为0
        int percent = (int) (((float) mProgress / (float) mMaxProgress) * 100);
        if (mTextIsDisplayable && percent != 0) {
            mPaint.setStrokeWidth(0);
            mPaint.setColor(mTextColor);
            mPaint.setTextSize(mTextSize);
            // 设置字体
            mPaint.setTypeface(Typeface.DEFAULT_BOLD);
            // 测量字体宽度，我们需要根据字体的宽度设置在圆环中间
            float textWidth = mPaint.measureText(percent + "%");
            // 画出进度百分比
            canvas.drawText(percent + "%", mCenterX - textWidth / 2, mCenterX
                    + mTextSize / 2, mPaint);
        }
        switch (mStyle) {
        // 连续实心
        case LINK_FILL:
            mPaint.setStyle(Paint.Style.FILL_AND_STROKE);// 填充内部和边缘
            mPaint.setStrokeWidth(mSectionHeight);// 设置圆环（进度条）的宽度
            mPaint.setColor(mRoundProgressStartColor);
            mSectionRect = new RectF(mCenterX - mRadius, mCenterX - mRadius,
                    mCenterX + mRadius, mCenterX + mRadius);
            canvas.drawArc(mSectionRect, -90, 360 * mProgress / mMaxProgress,
                    false, mPaint);// 画弧
            break;
        // 连续空心
        case LINK_STROKE:
            mPaint.setStyle(Paint.Style.STROKE);// 仅描边
            mPaint.setStrokeWidth(mSectionHeight);
            mPaint.setColor(mRoundProgressStartColor);
            mSectionRect = new RectF(mCenterX - mRadius, mCenterX - mRadius,
                    mCenterX + mRadius, mCenterX + mRadius);
            canvas.drawArc(mSectionRect, -90, 360 * mProgress / mMaxProgress,
                    false, mPaint);
            break;
        // 间隔大
        case SPACE_BIG:
            mPaint.setStyle(Paint.Style.FILL);// 填充内部
            canvas.translate(mCenterX, mCenterY);// 把正中间点当原点 平移到正中间
                                                 // 在x轴移动mCenterX 在y轴移动mCenterY
            if (mProgress <= mMaxProgress) {
                for (int i = 0; i < mProgress; ++i) {
                    canvas.save();// 保存之前所画的
                    canvas.rotate((float) i * rotation);// 画布旋转相对应的角度
                    canvas.translate(0, -mRadius); // 在x轴移动0 在y轴移动-mRadius
                    // 颜色渐变 设置画笔颜色
                    float bias = (float) i / (float) (mMaxProgress - 1);
                    int color = interpolateColor(mRoundProgressStartColor,
                            mRoundProgressFinishColor, bias);
                    mPaint.setColor(color);
                    mSectionRect = new RectF(-mSectionWidth / 2,
                            -mSectionHeight / 2, mSectionWidth / 2,
                            mSectionHeight / 2);
                    canvas.drawRoundRect(mSectionRect, 3, 3, mPaint);// 画圆角矩形
                    canvas.restore();// 回滚到原来画布
                }
            }

            break;
        // 间隔小
        case SPACE_SMALL:
            mPaint.setStyle(Paint.Style.FILL);
            canvas.translate(mCenterX, mCenterY);
            if (mProgress <= mMaxProgress) {
                for (int i = 0; i < mProgress; ++i) {
                    canvas.save();
                    canvas.rotate((float) i * rotation);
                    canvas.translate(0, -mRadius);
                    float bias = (float) i / (float) (mMaxProgress - 1);
                    int color = interpolateColor(mRoundProgressStartColor,
                            mRoundProgressFinishColor, bias);
                    mPaint.setColor(color);
                    mSectionRect = new RectF(-mSectionWidth / 2,
                            -mSectionHeight / 2, mSectionWidth / 2,
                            mSectionHeight / 2);
                    canvas.drawRect(mSectionRect, mPaint);
                    canvas.restore();
                }
            }
            break;

        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int heigth = MeasureSpec.getSize(heightMeasureSpec);
        if (width > heigth) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            super.onMeasure(heightMeasureSpec, widthMeasureSpec);
        }
        updateDimensions(getWidth(), getHeight());

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        updateDimensions(w, h);
    }

    private void updateDimensions(int width, int height) {
        // Update center position
        mCenterX = width / 2.0f;
        mCenterY = height / 2.0f;
        int dimater = Math.min(width, height);
        float outerRadius = dimater / 2;
        mSectionHeight = outerRadius * mOuterRadiusHeightRatio;
        mSectionWidth = mSectionHeight / mHeightWidthRatio;
        mRadius = outerRadius - mSectionHeight / 2;
    }

    private float interpolate(float a, float b, float bias) {
        return (a + ((b - a) * bias));
    }

    private int interpolateColor(int colorA, int colorB, float bias) {
        float[] hsvColorA = new float[3];
        Color.colorToHSV(colorA, hsvColorA);

        float[] hsvColorB = new float[3];
        Color.colorToHSV(colorB, hsvColorB);

        hsvColorB[0] = interpolate(hsvColorA[0], hsvColorB[0], bias);
        hsvColorB[1] = interpolate(hsvColorA[1], hsvColorB[1], bias);
        hsvColorB[2] = interpolate(hsvColorA[2], hsvColorB[2], bias);
        // NOTE For some reason the method HSVToColor fail in edit mode. Just
        // use the start color for now
        if (isInEditMode())
            return colorA;

        return Color.HSVToColor(hsvColorB);
    }

    public int getRoundColor() {
        return mRoundColor;
    }

    public void setRoundColor(int mRoundColor) {
        this.mRoundColor = mRoundColor;
    }

    public int getRoundProgressStartColor() {
        return mRoundProgressStartColor;
    }

    public void setRoundProgressStartColor(int mRoundProgressStartColor) {
        this.mRoundProgressStartColor = mRoundProgressStartColor;
    }

    public int getRoundProgressFinishColor() {
        return mRoundProgressFinishColor;
    }

    public void setRoundProgressFinishColor(int mRoundProgressFinishColor) {
        this.mRoundProgressFinishColor = mRoundProgressFinishColor;
    }

    public int getTextColor() {
        return mTextColor;
    }

    public void setTextColor(int mTextColor) {
        this.mTextColor = mTextColor;
    }

    public int getTextSize() {
        return mTextSize;
    }

    public void setTextSize(int mTextSize) {
        this.mTextSize = mTextSize;
    }

    public synchronized int getMaxProgress() {
        return mMaxProgress;
    }

    public synchronized void setMaxProgress(int max) {
        int newMax = Math.max(max, 1);
        if (newMax != mMaxProgress)
            mMaxProgress = newMax;

        updateProgress(mProgress);
        postInvalidate();
    }

    public synchronized int getProgress() {
        return mProgress;
    }

    public synchronized void setProgress(int mProgress) {
        updateProgress(mProgress);
    }

    public boolean getTextIsDisplayable() {
        return mTextIsDisplayable;
    }

    public void setTextIsDisplayable(boolean mTextIsDisplayable) {
        this.mTextIsDisplayable = mTextIsDisplayable;
    }

    public int getStyle() {
        return mStyle;
    }

    public void setStyle(int mStyle) {
        this.mStyle = mStyle;
    }

    public float getHeightWidthRatio() {
        return mHeightWidthRatio;
    }

    public void setHeightWidthRatio(float mHeightWidthRatio) {
        this.mHeightWidthRatio = mHeightWidthRatio;
    }

    public float getOuterRadiusHeightRatio() {
        return mOuterRadiusHeightRatio;
    }

    public void setOuterRadiusHeightRatio(float mOuterRadiusHeightRatio) {
        this.mOuterRadiusHeightRatio = mOuterRadiusHeightRatio;
    }

    // 更新进度，此为线程安全控件，由于考虑多线程问题，需要同步 刷新界面调用postInvalidate()能在非UI线程刷新
    protected boolean updateProgress(int progress) {

        progress = Math.max(0, Math.min(mMaxProgress, progress));

        if (progress != mProgress) {
            mProgress = progress;
            postInvalidate();
            return true;
        }

        return false;
    }
}