/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.clock;

/**
 * 表盘属性
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class Clock {
    /**
     * 表盘的类型，数字表盘
     */
    public static final String TYPE_DIGITAL = "digital";

    /**
     * 表盘的类型，模拟表盘
     */
    public static final String TYPE_ANALOG = "analog";

    /**
     * 表盘目前含有2种数字表盘的 style,系统默认
     */
    public static final String NORMAL = "normal";

    /**
     * 经典
     */
    public static final String CLASSIC = "classic";

    /**
     * 极光
     */
    public static final String AURORA = "aurora";

    /**
     * 应用退出action
     */
    public static final String COM_INGENIC_DESTROY_ACTION = "com.ingenic.ondestroy";

    /**
     * 时区切换完毕action
     */
    public static final String COM_INGENIC_TIME_OVER_ACTION = "com.ingenic.timezone.over";

    /**
     * 加载表盘结束action
     */
    public static final String COM_INGENIC_READY_ACTION = "com.ingenic.clock.ready";

    /**
     * 表盘停止的动画，从运行时的表盘，切换到预览时的停止表盘，让时间回到默认设置的时间
     */
    public static final String COM_INGENIC_STOP_CLOCK_ANIMATION = "com.ingenic.stop.clock.animation";

    /**
     * 表盘从停止状态切换到运动状态，需要一个切换到实时时间的动画
     */
    public static final String COM_INGENIC_START_CLOCK_ANIMATION = "com.ingenic.start.clock.animation";

    /**
     * 正在运行的表盘的 clockId action
     */
    public static final String COM_INGENIC_RUNNING_CLOCK_ID_ACTION = "com.ingenic.running.clock.id.action";

    /**
     * 获取日出，日落的action
     */
    public static final String SUN_RISE_SET_ACTION = "com.ingenic.sun.rise.set.action";

    /**
     * 得到天气的信息
     */
    public static final String GET_WEATHER_INFO_ACTION = "com.ingenic.weather.info.action";

    /**
     * 得到天气的信息,flag
     */
    public static final String WEATHER_INFO_FLAG = "weather_info";
    /**
     * clock id 的flag
     */
    public static final String CLOCK_ID_FLAG = "clock_id";

    private String mClockId;

    // digital clock
    private String mFilePath;
    private String mType;
    private String mStyle;
    private String mBackground;

    private String mDisLunar;
    private String mLunarLocation;
    private String mHourLocation;
    private String mTimeSelocation;
    private String minuteLocation;
    private String monthDayWeekLocation;

    // analog clock
    private String mHourPoint;
    private String mMinutePoint;
    private String mSecondPoint;
    private String mMonthBg;
    private String mMonthPoint;
    private String mWeekBg;
    private String mWeekPoint;
    private String mAmpmBg;
    private String mAmpmPoint;

    private String mDayNoteLocation;
    private String mAmpmNotelocation;
    private String mWeekNotelocation;
    private String mClockNameValues;

    private String mPreclock;

    private String mSecondProgress;
    private String mMinuteProgress;
    private String mHourProgress;

    /**
     * 月相
     */
    private String mMoonName;
    private String mMoonView;

    /**
     * 日出
     */
    private String mSunriseNote;
    private String mSunriseBg;

    /**
     * 日落
     */
    private String mSunsetNote;
    private String mSunsetBg;

    /**
     * 天气
     */
    private String mWeatherNote;

    public String getmSunsetNote() {
        return mSunsetNote;
    }

    public void setmSunsetNote(String mSunsetNote) {
        this.mSunsetNote = mSunsetNote;
    }

    public String getmSunsetBg() {
        return mSunsetBg;
    }

    public void setmSunsetBg(String mSunsetBg) {
        this.mSunsetBg = mSunsetBg;
    }

    public String getmWeatherNote() {
        return mWeatherNote;
    }

    public void setmWeatherNote(String mWeather) {
        this.mWeatherNote = mWeather;
    }

    public String getmMoonName() {
        return mMoonName;
    }

    public void setmMoonName(String mMoonName) {
        this.mMoonName = mMoonName;
    }

    public String getmMoonView() {
        return mMoonView;
    }

    public void setmMoonView(String mMoonView) {
        this.mMoonView = mMoonView;
    }

    public String getmSunriseNote() {
        return mSunriseNote;
    }

    public void setmSunriseNote(String mSunriseNote) {
        this.mSunriseNote = mSunriseNote;
    }

    public String getmSunriseBg() {
        return mSunriseBg;
    }

    public void setmSunriseBg(String mSunriseBg) {
        this.mSunriseBg = mSunriseBg;
    }

    public String getmClockId() {
        return mClockId;
    }

    public void setmClockId(String mClockId) {
        this.mClockId = mClockId;
    }

    public String getmSecondProgress() {
        return mSecondProgress;
    }

    public void setmSecondProgress(String mSecondProgress) {
        this.mSecondProgress = mSecondProgress;
    }

    public String getmMinuteProgress() {
        return mMinuteProgress;
    }

    public void setmMinuteProgress(String mMinuteProgress) {
        this.mMinuteProgress = mMinuteProgress;
    }

    public String getmHourProgress() {
        return mHourProgress;
    }

    public void setmHourProgress(String mHourProgress) {
        this.mHourProgress = mHourProgress;
    }

    public String getmClockNameValues() {
        return mClockNameValues;
    }

    public void setmClockNameValues(String mClockNameValues) {
        this.mClockNameValues = mClockNameValues;
    }

    public String getmPreclock() {
        return mPreclock;
    }

    public void setmPreclock(String mPreclock) {
        this.mPreclock = mPreclock;
    }

    public String getmAmpmNotelocation() {
        return mAmpmNotelocation;
    }

    public void setmAmpmNotelocation(String mAmpmNotelocation) {
        this.mAmpmNotelocation = mAmpmNotelocation;
    }

    public String getmWeekNotelocation() {
        return mWeekNotelocation;
    }

    public void setmWeekNotelocation(String mWeekNotelocation) {
        this.mWeekNotelocation = mWeekNotelocation;
    }

    public String getmHourPoint() {
        return mHourPoint;
    }

    public void setmHourPoint(String mHourPoint) {
        this.mHourPoint = mHourPoint;
    }

    public String getmMinutePoint() {
        return mMinutePoint;
    }

    public void setmMinutePoint(String mMinutePoint) {
        this.mMinutePoint = mMinutePoint;
    }

    public String getmSecondPoint() {
        return mSecondPoint;
    }

    public void setmSecondPoint(String mSecondPoint) {
        this.mSecondPoint = mSecondPoint;
    }

    public String getmMonthBg() {
        return mMonthBg;
    }

    public void setmMonthBg(String mMonthBg) {
        this.mMonthBg = mMonthBg;
    }

    public String getmMonthPoint() {
        return mMonthPoint;
    }

    public void setmMonthPoint(String mMonthPoint) {
        this.mMonthPoint = mMonthPoint;
    }

    public String getmWeekBg() {
        return mWeekBg;
    }

    public void setmWeekBg(String mWeekBg) {
        this.mWeekBg = mWeekBg;
    }

    public String getmWeekPoint() {
        return mWeekPoint;
    }

    public void setmWeekPoint(String mWeekPoint) {
        this.mWeekPoint = mWeekPoint;
    }

    public String getmAmpmBg() {
        return mAmpmBg;
    }

    public void setmAmpmBg(String mAmpmBg) {
        this.mAmpmBg = mAmpmBg;
    }

    public String getmAmpmPoint() {
        return mAmpmPoint;
    }

    public void setmAmpmPoint(String mAmpmPoint) {
        this.mAmpmPoint = mAmpmPoint;
    }

    public String getmDayNoteLocation() {
        return mDayNoteLocation;
    }

    public void setmDayNoteLocation(String mDayNoteLocation) {
        this.mDayNoteLocation = mDayNoteLocation;
    }

    public String getDisLunar() {
        return mDisLunar;
    }

    public void setDisLunar(String mDisLunar) {
        this.mDisLunar = mDisLunar;
    }

    public String getLunarLocation() {
        return mLunarLocation;
    }

    public void setLunarLocation(String mLunarLocation) {
        this.mLunarLocation = mLunarLocation;
    }

    public String getHourLocation() {
        return mHourLocation;
    }

    public void setHourLocation(String mHourLocation) {
        this.mHourLocation = mHourLocation;
    }

    public String getTimeSelocation() {
        return mTimeSelocation;
    }

    public void setTimeSelocation(String mTimeSelocation) {
        this.mTimeSelocation = mTimeSelocation;
    }

    public String getMinuteLocation() {
        return minuteLocation;
    }

    public void setMinuteLocation(String minuteLocation) {
        this.minuteLocation = minuteLocation;
    }

    public String getMonthDayWeekLocation() {
        return monthDayWeekLocation;
    }

    public void setMonthDayWeekLocation(String monthDayWeekLocation) {
        this.monthDayWeekLocation = monthDayWeekLocation;
    }

    private String mTimeColor;

    public void setmTimeColor(String mTimeColor) {
        this.mTimeColor = mTimeColor;
    }

    public String getmTimeColor() {
        return mTimeColor;
    }

    public void setTimeColor(String mTimeColor) {
        this.mTimeColor = mTimeColor;
    }

    private String weekviewcolor;

    public String getWeekviewcolor() {
        return weekviewcolor;
    }

    public void setWeekviewcolor(String weekviewcolor) {
        this.weekviewcolor = weekviewcolor;
    }

    public String getmFilePath() {
        return mFilePath;
    }

    public void setmFilePath(String mFilePath) {
        this.mFilePath = mFilePath;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmStyle() {
        return mStyle;
    }

    public void setmStyle(String mStyle) {
        this.mStyle = mStyle;
    }

    public String getmBackground() {
        return mBackground;
    }

    public void setmBackground(String mBackground) {
        this.mBackground = mBackground;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        return mFilePath.equals(((Clock) o).getmFilePath());
    }

    @Override
    public String toString() {

        return "filePath:" + mFilePath + "   type:" + mType + "   style:"
                + mStyle + "    background:" + mBackground;
    }

}
