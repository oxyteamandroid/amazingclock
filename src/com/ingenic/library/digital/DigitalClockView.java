/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.digital;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingenic.library.clock.ChineseLunar;
import com.ingenic.library.clock.Clock;
import com.ingenic.library.clock.ClockProperty;
import com.ingenic.library.clock.ClockUtils;
import com.ingenic.library.clock.ClockView;
import com.ingenic.library.clock.R;

/**
 * 数字表盘
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class DigitalClockView extends ClockView {

    private View mView = null;

    private FrameLayout mRootView;
    private TextView mLunarView; // 显示农历
    private TextView mHourTime; // 显示 小时
    private TextView mTimeSeparated; // 显示 小时 和 分 之间的分隔符号
    private TextView mMinutesTime; // 显示 分

    private TextView mMonthWeekDayTime; // 显示月份 周 几号

    private TextView mDigitalClockNameView = null; // 显示表盘名称的view

    // private Time time;

    private String mTimeZoneId;

    String[] mClockNameValue = null;

    private LayoutInflater inflater = null;
    private boolean mIsWorldCity = false;

    /**
     * 正在运行的表盘
     */
    private String mRunningClockId;

    public static int mDisplayNowTimeState = ClockProperty.CLOCK_TIME_NORMAL;

    public DigitalClockView(Context context) {
        super(context);
        mContext = context;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = inflater.inflate(R.layout.library_digital_type, this);

        mRootView = (FrameLayout) mView.findViewById(R.id.view_root);
        mLunarView = (TextView) mView.findViewById(R.id.lunarView);
        mHourTime = (TextView) mView.findViewById(R.id.hour_time);
        mTimeSeparated = (TextView) mView.findViewById(R.id.time_separated);
        mMinutesTime = (TextView) mView.findViewById(R.id.minutes_time);
        mMonthWeekDayTime = (TextView) mView
                .findViewById(R.id.monthweekday_time);
        mDigitalClockNameView = (TextView) mView
                .findViewById(R.id.clock_name_text);

        // time = new Time();

    }

    @Override
    protected void onDetachedFromWindow() {
        // TODO Auto-generated method stub
        super.onDetachedFromWindow();
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case ClockProperty.CLOCK_SCALE_SMALL:
                updateTime();
                break;
            case ClockProperty.CLOCK_SCALE_NORMAL:
                if (mRunningClockId != null
                        && mRunningClockId.equals(clock.getmClockId())) {
                    updateTime();
                } else if (clock != null && clock.getmClockId() == null) {
                    updateTime();
                }
                break;
            default:
                break;
            }
        };
    };

    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Clock.COM_INGENIC_DESTROY_ACTION.equals(action)) {
                try {
                    context.unregisterReceiver(mIntentReceiver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                recycleBitmap();
            } else if (Clock.COM_INGENIC_TIME_OVER_ACTION.equals(action)) {
                if (mIsWorldCity) {
                    try {
                        context.unregisterReceiver(mIntentReceiver);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    recycleBitmap();
                }
            } else if (Clock.COM_INGENIC_STOP_CLOCK_ANIMATION.equals(action)) {
                mDisplayNowTimeState = ClockProperty.CLOCK_SCALE_SMALL;
                mHandler.removeMessages(ClockProperty.CLOCK_SCALE_SMALL);
                mHandler.sendEmptyMessageDelayed(
                        ClockProperty.CLOCK_SCALE_SMALL,
                        ClockProperty.ANIMATION_SCALE_DURATION);
            } else if (Clock.COM_INGENIC_RUNNING_CLOCK_ID_ACTION.equals(action)) {
                mRunningClockId = intent.getStringExtra(Clock.CLOCK_ID_FLAG);
            } else if (Clock.COM_INGENIC_START_CLOCK_ANIMATION.equals(action)) {
                mDisplayNowTimeState = ClockProperty.CLOCK_SCALE_NORMAL;

                mHandler.removeMessages(ClockProperty.CLOCK_SCALE_NORMAL);
                mHandler.sendEmptyMessageDelayed(
                        ClockProperty.CLOCK_SCALE_NORMAL,
                        ClockProperty.ANIMATION_SCALE_DURATION);
            } else {
                if (mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_NORMAL) {
                    mDisplayNowTimeState = ClockProperty.CLOCK_TIME_NORMAL;
                }

                if (mDisplayNowTimeState == ClockProperty.CLOCK_TIME_NORMAL) {
                    if (mRunningClockId != null
                            && mRunningClockId.equals(clock.getmClockId())) {
                        updateTime();
                    } else if (clock != null && clock.getmClockId() == null) {
                        updateTime();
                    }
                }
            }
        }
    };

    private void initFilter() {
        IntentFilter filter = new IntentFilter();

        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Clock.COM_INGENIC_READY_ACTION);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        filter.addAction(Intent.ACTION_CONFIGURATION_CHANGED);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_DATE_CHANGED);
        filter.addAction(Intent.ACTION_LOCALE_CHANGED);
        filter.addAction(Clock.COM_INGENIC_DESTROY_ACTION);
        filter.addAction(Clock.COM_INGENIC_TIME_OVER_ACTION);
        filter.addAction(Clock.COM_INGENIC_STOP_CLOCK_ANIMATION);
        filter.addAction(Clock.COM_INGENIC_START_CLOCK_ANIMATION);
        filter.addAction(Clock.COM_INGENIC_RUNNING_CLOCK_ID_ACTION);
        mContext.registerReceiver(mIntentReceiver, filter);

    }

    private String getMonthNumger(int month) {
        String[] months = ((Activity) mContext).getResources().getStringArray(
                R.array.library_month_entries);
        return months[month - 1];
    }

    private String getDayOfWeek(int dayOfWeek) {
        String[] days = ((Activity) mContext).getResources().getStringArray(
                R.array.library_day_of_week_entries);
        if (dayOfWeek <= 0 || dayOfWeek > days.length) {
            return days[0];
        } else {
            return days[dayOfWeek - 1];
        }
    }

    public void setTimeZone(String id) {
        mTimeZoneId = id;
    }

    public void setWorldCity(boolean isWorldCity) {
        mIsWorldCity = isWorldCity;
    }

    public void setDigitalClock(Clock c) {
        clock = c;
        initFilter();
        initView();
    }

    public FrameLayout getRootView() {
        return mRootView;
    }

    private void initView() {
        super.initDefaultView(mRootView);
        try {
            try {
                if (clock.getDisLunar().equals("true")
                        && Locale.getDefault().toString().equals("zh_CN")) {
                    mLunarView.setVisibility(View.VISIBLE);

                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                            LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT);
                    lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    mLunarView.setLayoutParams(lp);
                    String[] lunarStr = null;
                    if (clock.getLunarLocation() != null) {
                        lunarStr = clock.getLunarLocation().split(",");
                    }

                    if (lunarStr != null && lunarStr != null
                            && lunarStr.length > ClockProperty.LUNAR_FONT) {
                        mLunarView
                                .setY(Integer
                                        .parseInt(lunarStr[ClockProperty.LUNAR_Y_VALUE]));
                        mLunarView
                                .setTextSize(Float
                                        .parseFloat(lunarStr[ClockProperty.LUNAR_FONT_SIZE]));
                        mLunarView
                                .setTextColor(Color
                                        .parseColor(lunarStr[ClockProperty.LUNAR_FONT_COLOR]));

                        if (lunarStr.length > ClockProperty.LUNAR_FONT
                                && !lunarStr[ClockProperty.LUNAR_FONT]
                                        .equals("")) {
                            try {
                                Typeface luarNoteFace = Typeface
                                        .createFromFile(clock.getmFilePath()
                                                + "/"
                                                + lunarStr[ClockProperty.LUNAR_FONT]);
                                mLunarView.setTypeface(luarNoteFace);
                            } catch (Exception e) {
                                // TODO: handle exception
                            }
                        }
                    }
                } else {
                    mLunarView.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                // TODO: handle exception
            }
            String[] timeSeparted = null;
            if (clock.getTimeSelocation() != null) {
                timeSeparted = clock.getTimeSelocation().split(",");
            }

            if (timeSeparted.length > ClockProperty.TIME_NOTE_FONT) {
                if (Integer
                        .parseInt(timeSeparted[ClockProperty.TIME_NOTE_X_VALUE]) > 0) {
                    mTimeSeparated.setVisibility(View.VISIBLE);
                    mTimeSeparated
                            .setX(Integer
                                    .parseInt(timeSeparted[ClockProperty.TIME_NOTE_X_VALUE]));
                    mTimeSeparated
                            .setY(Integer
                                    .parseInt(timeSeparted[ClockProperty.TIME_NOTE_Y_VALUE]));
                    mTimeSeparated
                            .setTextSize(Float
                                    .parseFloat(timeSeparted[ClockProperty.TIME_NOTE_FONT_SIZE]));
                    mTimeSeparated
                            .setTextColor(Color
                                    .parseColor(timeSeparted[ClockProperty.TIME_NOTE_FONT_COLOR]));
                    if (timeSeparted.length > ClockProperty.TIME_NOTE_FONT
                            && !timeSeparted[ClockProperty.TIME_NOTE_FONT]
                                    .equals("")) {
                        try {
                            Typeface timeNoteFace = Typeface
                                    .createFromFile(clock.getmFilePath()
                                            + "/"
                                            + timeSeparted[ClockProperty.TIME_NOTE_FONT]);
                            mTimeSeparated.setTypeface(timeNoteFace);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                    } else {
                        Typeface tf = Typeface.createFromAsset(
                                mContext.getAssets(), "fonts/dclassic.ttf");
                        mTimeSeparated.setTypeface(tf);
                    }

                    String[] timeHour = null;
                    if (clock.getHourLocation() != null) {
                        timeHour = clock.getHourLocation().split(",");
                    }

                    if (timeHour.length > ClockProperty.HOUR_FONT) {
                        mHourTime.setVisibility(View.VISIBLE);
                        if (Clock.AURORA.equals(clock.getmStyle())) {
                            mHourTime
                                    .setY(Integer
                                            .parseInt(timeHour[ClockProperty.HOUR_Y_VALUE]));
                        } else {
                            mHourTime
                                    .setX(Integer
                                            .parseInt(timeHour[ClockProperty.HOUR_X_VALUE]));
                            mHourTime
                                    .setY(Integer
                                            .parseInt(timeHour[ClockProperty.HOUR_Y_VALUE]));
                        }
                        mHourTime
                                .setTextSize(Float
                                        .parseFloat(timeHour[ClockProperty.HOUR_FONT_SIZE]));
                        mHourTime
                                .setTextColor(Color
                                        .parseColor(timeHour[ClockProperty.HOUR_FONT_COLOR]));
                        if (timeHour.length > ClockProperty.HOUR_FONT
                                && !timeHour[ClockProperty.HOUR_FONT]
                                        .equals("")) {
                            try {
                                Typeface hourNoteFace = Typeface
                                        .createFromFile(clock.getmFilePath()
                                                + "/"
                                                + timeHour[ClockProperty.HOUR_FONT]);
                                mHourTime.setTypeface(hourNoteFace);
                            } catch (Exception e) {
                                // TODO: handle exception
                            }
                        } else {
                            Typeface tf = Typeface.createFromAsset(
                                    mContext.getAssets(),
                                    "fonts/time_fonts.ttf");
                            mHourTime.setTypeface(tf);
                        }
                    } else {
                        mHourTime.setVisibility(View.GONE);
                    }

                    String[] timeMinute = null;
                    if (clock.getMinuteLocation() != null) {
                        timeMinute = clock.getMinuteLocation().split(",");
                    }

                    if (timeMinute.length > ClockProperty.MINUTE_FONT) {
                        mMinutesTime.setVisibility(View.VISIBLE);
                        mMinutesTime
                                .setX(Integer
                                        .parseInt(timeMinute[ClockProperty.MINUTE_X_VALUE]));
                        mMinutesTime
                                .setY(Integer
                                        .parseInt(timeMinute[ClockProperty.MINUTE_Y_VALUE]));
                        mMinutesTime
                                .setTextSize(Float
                                        .parseFloat(timeMinute[ClockProperty.MINUTE_FONT_SIZE]));
                        mMinutesTime
                                .setTextColor(Color
                                        .parseColor(timeMinute[ClockProperty.MINUTE_FONT_COLOR]));

                        if (timeMinute.length > ClockProperty.MINUTE_FONT
                                && !timeMinute[ClockProperty.MINUTE_FONT]
                                        .equals("")) {
                            try {
                                Typeface minuNoteFace = Typeface
                                        .createFromFile(clock.getmFilePath()
                                                + "/"
                                                + timeMinute[ClockProperty.MINUTE_FONT]);
                                mMinutesTime.setTypeface(minuNoteFace);
                            } catch (Exception e) {
                                // TODO: handle exception
                            }
                        } else {
                            Typeface tf = Typeface.createFromAsset(
                                    mContext.getAssets(),
                                    "fonts/time_fonts.ttf");
                            mMinutesTime.setTypeface(tf);
                        }
                    } else {
                        mMinutesTime.setVisibility(View.GONE);
                    }

                } else {
                    mTimeSeparated.setVisibility(View.GONE);

                    RelativeLayout.LayoutParams lpH = new RelativeLayout.LayoutParams(
                            LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT);
                    lpH.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    mHourTime.setLayoutParams(lpH);

                    RelativeLayout.LayoutParams lpM = new RelativeLayout.LayoutParams(
                            LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT);
                    lpM.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    mMinutesTime.setLayoutParams(lpM);
                    mHourTime.setTextAppearance(mContext,
                            R.style.text_bold_style);

                    String[] timeHour = null;
                    if (clock.getHourLocation() != null) {
                        timeHour = clock.getHourLocation().split(",");
                    }
                    if (timeHour.length > ClockProperty.HOUR_FONT) {
                        mHourTime.setVisibility(View.VISIBLE);
                        mHourTime
                                .setY(Integer
                                        .parseInt(timeHour[ClockProperty.HOUR_Y_VALUE]));
                        mHourTime
                                .setTextSize(Float
                                        .parseFloat(timeHour[ClockProperty.HOUR_FONT_SIZE]));
                        mHourTime
                                .setTextColor(Color
                                        .parseColor(timeHour[ClockProperty.HOUR_FONT_COLOR]));

                        if (timeHour.length > ClockProperty.HOUR_FONT
                                && !timeHour[ClockProperty.HOUR_FONT]
                                        .equals("")) {
                            try {
                                Typeface hourNoteFace = Typeface
                                        .createFromFile(clock.getmFilePath()
                                                + "/"
                                                + timeHour[ClockProperty.HOUR_FONT]);
                                mHourTime.setTypeface(hourNoteFace);
                            } catch (Exception e) {
                                // TODO: handle exception
                            }
                        } else {
                            Typeface tf = Typeface.createFromAsset(
                                    mContext.getAssets(),
                                    "fonts/time_fonts.ttf");

                            mHourTime.setTypeface(tf);
                        }

                    } else {
                        mHourTime.setVisibility(View.GONE);
                    }

                    String[] timeMinute = null;
                    if (clock.getMinuteLocation() != null) {
                        timeMinute = clock.getMinuteLocation().split(",");
                    }

                    if (timeMinute.length > ClockProperty.MINUTE_FONT) {
                        mMinutesTime.setVisibility(View.VISIBLE);
                        mMinutesTime
                                .setY(Integer
                                        .parseInt(timeMinute[ClockProperty.MINUTE_Y_VALUE]));
                        mMinutesTime
                                .setTextSize(Float
                                        .parseFloat(timeMinute[ClockProperty.MINUTE_FONT_SIZE]));
                        mMinutesTime
                                .setTextColor(Color
                                        .parseColor(timeMinute[ClockProperty.MINUTE_FONT_COLOR]));

                        if (timeMinute.length > ClockProperty.MINUTE_FONT
                                && !timeMinute[ClockProperty.MINUTE_FONT]
                                        .equals("")) {
                            try {
                                Typeface minuNoteFace = Typeface
                                        .createFromFile(clock.getmFilePath()
                                                + "/"
                                                + timeMinute[ClockProperty.MINUTE_FONT]);
                                mHourTime.setTypeface(minuNoteFace);
                            } catch (Exception e) {
                                // TODO: handle exception
                            }
                        } else {
                            Typeface tf = Typeface.createFromAsset(
                                    mContext.getAssets(),
                                    "fonts/time_fonts.ttf");

                            mMinutesTime.setTypeface(tf);
                        }

                    } else {
                        mMinutesTime.setVisibility(View.GONE);
                    }

                }

            } else {
                mTimeSeparated.setVisibility(View.GONE);
            }

            String[] timeMonthWeekDay = null;
            if (clock.getMonthDayWeekLocation() != null) {
                timeMonthWeekDay = clock.getMonthDayWeekLocation().split(",");
            }

            if (timeMonthWeekDay.length > ClockProperty.MONTH_WEEK_FONT) {
                mMonthWeekDayTime.setVisibility(View.VISIBLE);

                if (Integer.parseInt(timeMonthWeekDay[0]) == 0) {
                    RelativeLayout.LayoutParams lpMWD = new RelativeLayout.LayoutParams(
                            LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT);
                    lpMWD.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    mMonthWeekDayTime.setLayoutParams(lpMWD);
                    mMonthWeekDayTime
                            .setY(Integer
                                    .parseInt(timeMonthWeekDay[ClockProperty.MONTH_WEEK_Y_VALUE]));

                } else {
                    mMonthWeekDayTime
                            .setX(Integer
                                    .parseInt(timeMonthWeekDay[ClockProperty.MONTH_WEEK_X_VALUE]));
                    mMonthWeekDayTime
                            .setY(Integer
                                    .parseInt(timeMonthWeekDay[ClockProperty.MONTH_WEEK_Y_VALUE]));
                }

                mMonthWeekDayTime
                        .setTextSize(Float
                                .parseFloat(timeMonthWeekDay[ClockProperty.MONTH_WEEK_FONT_SIZE]));
                mMonthWeekDayTime
                        .setTextColor(Color
                                .parseColor(timeMonthWeekDay[ClockProperty.MONTH_WEEK_FONT_COLOR]));

                if (timeMonthWeekDay.length > ClockProperty.MONTH_WEEK_FONT
                        && !timeMonthWeekDay[ClockProperty.MONTH_WEEK_FONT]
                                .equals("")) {
                    try {
                        Typeface monWeekNoteFace = Typeface
                                .createFromFile(clock.getmFilePath()
                                        + "/"
                                        + timeMonthWeekDay[ClockProperty.MONTH_WEEK_FONT]);
                        mMonthWeekDayTime.setTypeface(monWeekNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                } else {

                    Typeface tf = Typeface.createFromAsset(
                            mContext.getAssets(), "fonts/time_fonts.ttf");
                    mMonthWeekDayTime.setTypeface(tf);

                }

            } else {
                mMonthWeekDayTime.setVisibility(View.GONE);
            }

            if (clock.getmClockNameValues() != null) {
                mClockNameValue = clock.getmClockNameValues().split(",");
            }

            // 添加表盘名称
            if (mClockNameValue != null
                    && mClockNameValue.length > ClockProperty.CLOCK_DISPALY_NAME_COLOR) {

                mDigitalClockNameView
                        .setTextSize(Integer
                                .parseInt(mClockNameValue[ClockProperty.CLOCK_DISPALY_NAME_SIZE]));
                mDigitalClockNameView
                        .setTextColor(Color
                                .parseColor(mClockNameValue[ClockProperty.CLOCK_DISPALY_NAME_COLOR]));
                mDigitalClockNameView.setText(ClockUtils.getLocalClockName(
                        mClockNameValue, mContext));
            }

            updateTime();

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    @Override
    public void updateTime() {

        String[] preClock = null;
        if (clock.getmPreclock() != null
                && mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {
            preClock = clock.getmPreclock().split(",");
        }

        Calendar mClockCalendar = Calendar.getInstance();

        mClockCalendar.setTimeInMillis(System.currentTimeMillis());

        if (!TextUtils.isEmpty(mTimeZoneId)) {
            mClockCalendar.setTimeZone(TimeZone.getTimeZone(mTimeZoneId));
        }

        // String month = (String) DateFormat.format("MM", mClockCalendar);
        int month = Integer.parseInt(DateFormat.format("MM", mClockCalendar)
                .toString());
        String hour = DateFormat.format("kk", mClockCalendar).toString();
        String minute = DateFormat.format("mm", mClockCalendar).toString();

        int weekday = mClockCalendar.get(Calendar.DAY_OF_WEEK);// time.weekDay;

        if (mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {

            if (preClock != null
                    && preClock.length > ClockProperty.PRE_CLOCK_HOUR) {
                mHourTime.setText(ClockUtils.formatClockTime(Integer
                        .parseInt(preClock[ClockProperty.PRE_CLOCK_HOUR])));
            }
            if (preClock != null
                    && preClock.length > ClockProperty.PRE_CLOCK_MINUTE) {
                mMinutesTime.setText(ClockUtils.formatClockTime(Integer
                        .parseInt(preClock[ClockProperty.PRE_CLOCK_MINUTE])));
            }
        } else {
            mHourTime.setText(hour);
            mMinutesTime.setText(minute);
        }

        if (Clock.NORMAL.equals(clock.getmStyle())) {
            // 系统默认的数字表盘
            if (mLunarView.getVisibility() == View.VISIBLE) {
                Calendar c = Calendar.getInstance();
                c.setTime(new java.util.Date());
                ChineseLunar mIngenicLunar = new ChineseLunar(c);
                mLunarView.setText(mIngenicLunar.toString());
            }

            int day = mClockCalendar.get(Calendar.DAY_OF_MONTH);

            mMonthWeekDayTime.setText(String.format("%02d", month) + ""
                    + mContext.getString(R.string.month_note)
                    + String.format("%02d", day) + ""
                    + mContext.getString(R.string.day_note) + "  "
                    + getDayOfWeek(weekday));

        } else if (Clock.CLASSIC.equals(clock.getmStyle())) {
            // 数字表盘 经典
            mMonthWeekDayTime.setText(getDayOfWeek(weekday) + ","
                    + getMonthNumger(month));

        } else if (Clock.AURORA.equals(clock.getmStyle())) {
            // 数字表盘 极光
            int day = mClockCalendar.get(Calendar.DAY_OF_MONTH);
            mMonthWeekDayTime.setText(getDayOfWeek(weekday) + "\n"
                    + String.format("%02d", month) + ""
                    + mContext.getString(R.string.month_note)
                    + String.format("%02d", day) + ""
                    + mContext.getString(R.string.day_note));
        }
    }
}
