/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.analog;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingenic.library.clock.ChineseLunar;
import com.ingenic.library.clock.Clock;
import com.ingenic.library.clock.ClockProperty;
import com.ingenic.library.clock.ClockUtils;
import com.ingenic.library.clock.ClockView;
import com.ingenic.library.clock.PointHourProgress;
import com.ingenic.library.clock.PointHourView;
import com.ingenic.library.clock.PointMinuteProgress;
import com.ingenic.library.clock.PointMinuteView;
import com.ingenic.library.clock.PointSecondProgress;
import com.ingenic.library.clock.PointSecondsView;
import com.ingenic.library.clock.R;

/**
 * 模拟表盘
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class AnalogClockView extends ClockView {
    private PointHourView mPointHour;
    private PointMinuteView mPointMinute;
    private PointSecondsView mPointSeconds;

    private PointSecondProgress mPointSecondProgress;
    private PointMinuteProgress mPointMinuteProgress;
    private PointHourProgress mPointHourProgress;

    private AnalogMonthView mMonthView; // 显示月份
    private AnalogWeekView mWeekView; // 显示周
    private AnalogAmPmView mAmPmView; // 显示上午下午

    private TextView mAnalogAmPmNoteView = null; // 设置上午下午的提示内容
    private TextView mAnalogDayNoteView = null; // 设置日期的提示内容
    private TextView mAnalogWeekNoteView = null; // 设置周的提示内容
    private TextView mAnalogHourNoteView = null; // 模拟表盘显示时的提示内容
    private TextView mAnalogMinuteNoteView = null; // 模拟表盘显示分的提示内容

    private TextView mMoonNameView = null; // 显示月相的名称
    private ImageView mMoonViewBg = null; // 显示月相的背景

    private TextView mSunRiseNote = null; // 日出的提示名称
    private ImageView mSunRiseBg = null; // 日出的背景

    private TextView mSunSetNote = null; // 日落的提示名称
    private ImageView mSunSetBg = null; // 日落的提示背景

    private TextView mWeatherNote = null; // 天气的提示信息

    private TextView mTimeSeparatedNote = null; // 时分之间的间隔

    private String[] mAmPmNoteValues = null;
    private String[] mDayNoteValues = null;
    private String[] mWeekNoteValue = null;
    private String[] mClockNameValue = null;

    private String[] mMoonNoteValue = null; // 月相的提示
    private String[] mMoonBgValue = null; // 月相的背景
    private String[] mSunRiseNoteValue = null; // 日出的提示
    private String[] mSunRiseBgValue = null; // 日出的背景
    private String[] mSunSetNoteValue = null; // 日落的提示
    private String[] mSunSetBgValue = null; // 日落的背景
    private String[] mWeatherNoteValue = null; // 天气的提示

    private TextView mAnalogClockNameView = null; // 显示表盘名称的view

    /**
     * 月相中，日出的时间，默认为 早上6点
     */
    private int mSunRise = 60 * 6;
    /**
     * 月相中，日落的时间，默认为 晚上6点
     */
    private int mSunSet = 60 * 18;

    /**
     * 天气的信息
     */
    private String mWeatherInfo = "no weather";

    private SharedPreferences mPreferencesSunRiseSet;

    private FrameLayout mRootView;
    private RelativeLayout mClockViewLayout = null;

    private View mView;
    private LayoutInflater inflater = null;
    private boolean mIsWorldCity = false;

    /**
     * 正在运行的表盘
     */
    private String mRunningClockId;

    public static int mDisplayNowTimeState = ClockProperty.CLOCK_TIME_NORMAL;

    public AnalogClockView(Context context) {
        super(context);
        mContext = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.library_analog_type, this);
        mRootView = (FrameLayout) mView.findViewById(R.id.view_root);
        mClockViewLayout = (RelativeLayout) mView.findViewById(R.id.view_clock);

        mAnalogClockNameView = (TextView) mView
                .findViewById(R.id.clock_name_text);

        mPreferencesSunRiseSet = mContext.getSharedPreferences(
                ClockProperty.SUN_RISE_SET_FLAG, Context.MODE_PRIVATE);
        mSunRise = mPreferencesSunRiseSet.getInt(ClockProperty.SUN_RISE_FLAG,
                mSunRise);
        mSunSet = mPreferencesSunRiseSet.getInt(ClockProperty.SUN_SET_FLAG,
                mSunSet);

    }

    public void setWorldCity(boolean isWorldCity) {
        mIsWorldCity = isWorldCity;
    }

    public void setAnalogClock(Clock c) {
        clock = c;
        initFilter();
        initView();
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case ClockProperty.CLOCK_SCALE_SMALL:
                updateTime();
                break;
            case ClockProperty.CLOCK_SCALE_NORMAL:
                if (clock != null && clock.getmClockId() != null
                        && mRunningClockId != null
                        && mRunningClockId.equals(clock.getmClockId())) {
                    updateTime();
                } else if (clock != null && clock.getmClockId() == null) {
                    updateTime();
                }
                break;

            default:
                break;
            }
        };
    };

    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Clock.COM_INGENIC_DESTROY_ACTION.equals(action)) {
                try {
                    context.unregisterReceiver(mIntentReceiver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                recyCleBitmap();
            } else if (Clock.COM_INGENIC_TIME_OVER_ACTION.equals(action)) {
                if (mIsWorldCity) {
                    try {
                        context.unregisterReceiver(mIntentReceiver);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    recyCleBitmap();
                }
            } else if(Clock.GET_WEATHER_INFO_ACTION.equals(action)){
                mWeatherInfo = intent.getExtras().getString(Clock.WEATHER_INFO_FLAG, "");
                if (mWeatherNote != null)
                    mWeatherNote.setText(mWeatherInfo);
            } else if (Clock.SUN_RISE_SET_ACTION.equals(action)) {

                mSunRise = intent.getExtras().getInt(
                        ClockProperty.SUN_RISE_FLAG);
                mSunSet = intent.getExtras().getInt(ClockProperty.SUN_SET_FLAG);

                mPreferencesSunRiseSet.edit()
                        .putInt(ClockProperty.SUN_RISE_FLAG, mSunRise)
                        .putInt(ClockProperty.SUN_SET_FLAG, mSunSet).commit();

            } else if (Clock.COM_INGENIC_STOP_CLOCK_ANIMATION.equals(action)) {
                mDisplayNowTimeState = ClockProperty.CLOCK_SCALE_SMALL;
                mHandler.removeMessages(ClockProperty.CLOCK_SCALE_SMALL);
                mHandler.sendEmptyMessageDelayed(
                        ClockProperty.CLOCK_SCALE_SMALL,
                        ClockProperty.ANIMATION_SCALE_DURATION);

            } else if (Clock.COM_INGENIC_RUNNING_CLOCK_ID_ACTION.equals(action)) {
                mRunningClockId = intent.getStringExtra(Clock.CLOCK_ID_FLAG);
            } else if (Clock.COM_INGENIC_START_CLOCK_ANIMATION.equals(action)) {
                mDisplayNowTimeState = ClockProperty.CLOCK_SCALE_NORMAL;
                mHandler.removeMessages(ClockProperty.CLOCK_SCALE_NORMAL);
                mHandler.sendEmptyMessageDelayed(
                        ClockProperty.CLOCK_SCALE_NORMAL,
                        ClockProperty.ANIMATION_SCALE_DURATION);
            } else {

                if (mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_NORMAL) {
                    mDisplayNowTimeState = ClockProperty.CLOCK_TIME_NORMAL;
                }

                if (mDisplayNowTimeState == ClockProperty.CLOCK_TIME_NORMAL) {
                    if (mRunningClockId != null
                            && mRunningClockId.equals(clock.getmClockId())) {
                        updateTime();
                    } else if (clock != null && clock.getmClockId() == null) {
                        updateTime();
                    }
                }
            }
        }
    };

    private void initFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ClockProperty.SECOND_ACTION_UPDATE);
        filter.addAction(Clock.COM_INGENIC_DESTROY_ACTION);
        filter.addAction(Clock.COM_INGENIC_TIME_OVER_ACTION);
        filter.addAction(Clock.COM_INGENIC_STOP_CLOCK_ANIMATION);
        filter.addAction(Clock.COM_INGENIC_START_CLOCK_ANIMATION);
        filter.addAction(Clock.COM_INGENIC_RUNNING_CLOCK_ID_ACTION);
        filter.addAction(Clock.SUN_RISE_SET_ACTION);
        filter.addAction(Clock.GET_WEATHER_INFO_ACTION);
        mContext.registerReceiver(mIntentReceiver, filter);
    }

    public FrameLayout getRootView() {
        return mRootView;
    }

    @SuppressWarnings({ "deprecation", "deprecation", "deprecation" })
    private void initView() {
        super.initDefaultView(mClockViewLayout);
        String[] mMonthBgValues = null;
        String[] mWeekBgValues = null;
        String[] mAmPmBgValues = null;

        String[] mHourLocation = null;
        String[] mMinuteLocation = null;

        if (clock.getmMonthBg() != null)
            mMonthBgValues = clock.getmMonthBg().split(",");
        if (clock.getmWeekBg() != null)
            mWeekBgValues = clock.getmWeekBg().split(",");
        if (clock.getmAmpmBg() != null)
            mAmPmBgValues = clock.getmAmpmBg().split(",");
        if (clock.getmAmpmNotelocation() != null) {
            mAmPmNoteValues = clock.getmAmpmNotelocation().split(",");
        }

        if (clock.getmDayNoteLocation() != null) {
            mDayNoteValues = clock.getmDayNoteLocation().split(",");
        }
        if (clock.getmWeekNotelocation() != null) {
            mWeekNoteValue = clock.getmWeekNotelocation().split(",");
        }
        if (clock.getHourLocation() != null) {
            mHourLocation = clock.getHourLocation().split(",");
        }
        if (clock.getMinuteLocation() != null) {
            mMinuteLocation = clock.getMinuteLocation().split(",");
        }
        if (clock.getmClockNameValues() != null) {
            mClockNameValue = clock.getmClockNameValues().split(",");
        }
        if (clock.getmMoonName() != null) {
            mMoonNoteValue = clock.getmMoonName().split(",");
        }
        if (clock.getmMoonView() != null) {
            mMoonBgValue = clock.getmMoonView().split(",");
        }
        if (clock.getmSunriseNote() != null) {
            mSunRiseNoteValue = clock.getmSunriseNote().split(",");
        }
        if (clock.getmSunriseBg() != null) {
            mSunRiseBgValue = clock.getmSunriseBg().split(",");
        }
        if (clock.getmSunsetNote() != null) {
            mSunSetNoteValue = clock.getmSunsetNote().split(",");
        }
        if (clock.getmSunsetBg() != null) {
            mSunSetBgValue = clock.getmSunsetBg().split(",");
        }
        if (clock.getmWeatherNote() != null) {
            mWeatherNoteValue = clock.getmWeatherNote().split(",");
        }

        // 显示月份的视图
        if (mMonthBgValues != null
                && mMonthBgValues.length > ClockProperty.MONTH_HEIGHT) {
            if (mMonthBgValues[ClockProperty.MONTH_DISPLAY].equals("true")) {
                mMonthView = new AnalogMonthView(mContext, clock);
                RelativeLayout.LayoutParams lpMonth = new RelativeLayout.LayoutParams(
                        Integer.parseInt(mMonthBgValues[ClockProperty.MONTH_WIDTH]),
                        Integer.parseInt(mMonthBgValues[ClockProperty.MONTH_HEIGHT]));
                lpMonth.setMargins(
                        Integer.parseInt(mMonthBgValues[ClockProperty.MONTH_X_VALUE]),
                        Integer.parseInt(mMonthBgValues[ClockProperty.MONTH_Y_VALUE]),
                        0, 0);
                try {
                    setBackgroundView(mMonthView,
                            mMonthBgValues[ClockProperty.MONTH_BG_VALUE]);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    // e.printStackTrace();
                }
                mClockViewLayout.addView(mMonthView, lpMonth);
            }
        }

        // 显示周的视图

        if (mWeekBgValues != null
                && mWeekBgValues.length > ClockProperty.WEEK_BG_HEIGHT) {

            if (mWeekBgValues[ClockProperty.WEEK_BG_DISPLAY].equals("true")) {
                mWeekView = new AnalogWeekView(mContext, clock);
                RelativeLayout.LayoutParams lpWeek = new RelativeLayout.LayoutParams(
                        Integer.parseInt(mWeekBgValues[ClockProperty.WEEK_BG_WIDTH]),
                        Integer.parseInt(mWeekBgValues[ClockProperty.WEEK_BG_HEIGHT]));
                lpWeek.setMargins(
                        Integer.parseInt(mWeekBgValues[ClockProperty.WEEK_BG__X_VALUE]),
                        Integer.parseInt(mWeekBgValues[ClockProperty.WEEK_BG__Y_VALUE]),
                        0, 0);
                try {
                    setBackgroundView(mWeekView,
                            mWeekBgValues[ClockProperty.WEEK_BG_VALUE]);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    // e.printStackTrace();
                }
                mClockViewLayout.addView(mWeekView, lpWeek);
            }
        }

        // 显示上午下午的视图
        if (mAmPmBgValues != null
                && mAmPmBgValues.length > ClockProperty.AM_PM_HEIGHT) {
            if (mAmPmBgValues[ClockProperty.AM_PM_DISPLAY].equals("true")) {
                mAmPmView = new AnalogAmPmView(mContext, clock);
                RelativeLayout.LayoutParams lpAmPm = new RelativeLayout.LayoutParams(
                        Integer.parseInt(mAmPmBgValues[ClockProperty.AM_PM_WIDTH]),
                        Integer.parseInt(mAmPmBgValues[ClockProperty.AM_PM_HEIGHT]));
                lpAmPm.setMargins(
                        Integer.parseInt(mAmPmBgValues[ClockProperty.AM_PM_X_VALUE]),
                        Integer.parseInt(mAmPmBgValues[ClockProperty.AM_PM_Y_VALUE]),
                        0, 0);
                try {
                    setBackgroundView(mAmPmView,
                            mAmPmBgValues[ClockProperty.AM_PM_BG_VALUE]);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    // e.printStackTrace();
                }
                mClockViewLayout.addView(mAmPmView, lpAmPm);
            }
        }

        // 显示月相的背景
        if (mMoonBgValue != null
                && mMoonBgValue.length > ClockProperty.MOON_VIEW_BG_DISPLAY) {
            if (mMoonBgValue[ClockProperty.MOON_VIEW_BG_DISPLAY].equals("true")) {
                mMoonViewBg = new ImageView(mContext);
                RelativeLayout.LayoutParams lpMoonViewBg = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpMoonViewBg
                        .setMargins(
                                Integer.parseInt(mMoonBgValue[ClockProperty.MOON_VIEW_BG_X]),
                                Integer.parseInt(mMoonBgValue[ClockProperty.MOON_VIEW_BG_Y]),
                                0, 0);
                mMoonViewBg.setLayoutParams(lpMoonViewBg);
                mClockViewLayout.addView(mMoonViewBg, lpMoonViewBg);
            }
        }

        // 显示月相的提示内容
        if (mMoonNoteValue != null
                && mMoonNoteValue.length > ClockProperty.MOON_NAME_NOTE_DISPLAY) {
            if (mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_DISPLAY]
                    .equals("true")) {
                mMoonNameView = new TextView(mContext);
                RelativeLayout.LayoutParams lpMoonNameNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpMoonNameNote
                        .setMargins(
                                Integer.parseInt(mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_X]),
                                Integer.parseInt(mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_Y]),
                                0, 0);
                mMoonNameView.setLayoutParams(lpMoonNameNote);

                int moonNameX = Integer
                        .parseInt(mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_X]);
                if (moonNameX > 0) {
                    lpMoonNameNote
                            .setMargins(
                                    moonNameX,
                                    Integer.parseInt(mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_Y]),
                                    0, 0);
                } else {
                    lpMoonNameNote.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    mMoonNameView
                            .setY(Integer
                                    .parseInt(mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_Y]));
                }
                mMoonNameView
                        .setTextSize(Float
                                .parseFloat(mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_SIZE]));
                mMoonNameView
                        .setTextColor(Color
                                .parseColor(mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_COLOR]));
                if (!mMoonNoteValue[ClockProperty.MOON_NAME_NOTE_FONT]
                        .equals("")) {
                    try {
                        Typeface weekNoteFace = Typeface
                                .createFromFile(clock.getmFilePath()
                                        + "/"
                                        + mAmPmNoteValues[ClockProperty.MOON_NAME_NOTE_FONT]);
                        mMoonNameView.setTypeface(weekNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                mClockViewLayout.addView(mMoonNameView, lpMoonNameNote);
            }
        }

        // 日出的提示
        if (mSunRiseNoteValue != null
                && mSunRiseNoteValue.length > ClockProperty.SUN_RISE_NOTE_DISPLAY) {
            if (mSunRiseNoteValue[ClockProperty.SUN_RISE_NOTE_DISPLAY]
                    .equals("true")) {

                mSunRiseNote = new TextView(mContext);
                RelativeLayout.LayoutParams lpSunRiseNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpSunRiseNote
                        .setMargins(
                                Integer.parseInt(mSunRiseNoteValue[ClockProperty.SUN_RISE_NOTE_X]),
                                Integer.parseInt(mSunRiseNoteValue[ClockProperty.SUN_RISE_NOTE_Y]),
                                0, 0);
                mSunRiseNote.setLayoutParams(lpSunRiseNote);
                mSunRiseNote
                        .setTextSize(Float
                                .parseFloat(mSunRiseNoteValue[ClockProperty.SUN_RISE_NOTE_SIZE]));
                mSunRiseNote
                        .setTextColor(Color
                                .parseColor(mSunRiseNoteValue[ClockProperty.SUN_RISE_NOTE_COLOR]));
                if (!mSunRiseNoteValue[ClockProperty.SUN_RISE_NOTE_FONT]
                        .equals("")) {
                    try {
                        Typeface weekNoteFace = Typeface
                                .createFromFile(clock.getmFilePath()
                                        + "/"
                                        + mSunRiseNoteValue[ClockProperty.SUN_RISE_NOTE_FONT]);
                        mSunRiseNote.setTypeface(weekNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                mClockViewLayout.addView(mSunRiseNote, lpSunRiseNote);
            }
        }

        // 显示日出的背景
        if (mSunRiseBgValue != null
                && mSunRiseBgValue.length > ClockProperty.SUN_RISE_BG_DISPLAY) {
            if (mSunRiseBgValue[ClockProperty.SUN_RISE_BG_DISPLAY]
                    .equals("true")) {
                mSunRiseBg = new ImageView(mContext);
                RelativeLayout.LayoutParams lpSunriseViewBg = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

                lpSunriseViewBg
                        .setMargins(
                                Integer.parseInt(mSunRiseBgValue[ClockProperty.SUN_RISE_BG_X]),
                                Integer.parseInt(mSunRiseBgValue[ClockProperty.SUN_RISE_BG_Y]),
                                0, 0);
                mSunRiseBg.setLayoutParams(lpSunriseViewBg);

                if (mSunRiseBgValue != null
                        && mSunRiseBgValue.length > ClockProperty.SUN_RISE_BG_SOURCE) {
                    mSunRiseBg
                            .setBackground(new BitmapDrawable(
                                    ClockUtils
                                            .getSunRiseSetBg(
                                                    clock,
                                                    mSunRiseBgValue[ClockProperty.SUN_RISE_BG_SOURCE])));
                }
                mClockViewLayout.addView(mSunRiseBg, lpSunriseViewBg);
            }
        }

        // 日落的提示
        if (mSunSetNoteValue != null
                && mSunSetNoteValue.length > ClockProperty.SUN_SET_NOTE_DISPLAY) {
            if (mSunSetNoteValue[ClockProperty.SUN_SET_NOTE_DISPLAY]
                    .equals("true")) {

                mSunSetNote = new TextView(mContext);
                RelativeLayout.LayoutParams lpSunSetNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpSunSetNote
                        .setMargins(
                                Integer.parseInt(mSunSetNoteValue[ClockProperty.SUN_RET_NOTE_X]),
                                Integer.parseInt(mSunSetNoteValue[ClockProperty.SUN_RET_NOTE_Y]),
                                0, 0);
                mSunSetNote.setLayoutParams(lpSunSetNote);
                mSunSetNote
                        .setTextSize(Float
                                .parseFloat(mSunSetNoteValue[ClockProperty.SUN_SET_NOTE_SIZE]));
                mSunSetNote
                        .setTextColor(Color
                                .parseColor(mSunSetNoteValue[ClockProperty.SUN_SET_NOTE_COLOR]));
                if (!mSunSetNoteValue[ClockProperty.SUN_SET_NOTE_FONT]
                        .equals("")) {
                    try {
                        Typeface weekNoteFace = Typeface
                                .createFromFile(clock.getmFilePath()
                                        + "/"
                                        + mSunSetNoteValue[ClockProperty.SUN_SET_NOTE_FONT]);
                        mSunSetNote.setTypeface(weekNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                mClockViewLayout.addView(mSunSetNote, lpSunSetNote);
            }
        }

        // 显示日落的背景
        if (mSunSetBgValue != null
                && mSunSetBgValue.length > ClockProperty.SUN_SET_BG_DISPLAY) {
            if (mSunSetBgValue[ClockProperty.SUN_SET_BG_DISPLAY].equals("true")) {
                mSunSetBg = new ImageView(mContext);
                RelativeLayout.LayoutParams lpSunsetViewBg = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

                lpSunsetViewBg
                        .setMargins(
                                Integer.parseInt(mSunSetBgValue[ClockProperty.SUN_SET_BG_X]),
                                Integer.parseInt(mSunSetBgValue[ClockProperty.SUN_SET_BG_Y]),
                                0, 0);
                mSunSetBg.setLayoutParams(lpSunsetViewBg);

                if (mSunSetBgValue != null
                        && mSunSetBgValue.length > ClockProperty.SUN_SET_BG_SOURCE) {
                    mSunSetBg
                            .setBackground(new BitmapDrawable(
                                    ClockUtils
                                            .getSunRiseSetBg(
                                                    clock,
                                                    mSunSetBgValue[ClockProperty.SUN_SET_BG_SOURCE])));
                }
                mClockViewLayout.addView(mSunSetBg, lpSunsetViewBg);
            }
        }

        // 天气的提示
        if (mWeatherNoteValue != null
                && mWeatherNoteValue.length > ClockProperty.WEATHER_NOTE_DISPLAY) {
            if (mWeatherNoteValue[ClockProperty.WEATHER_NOTE_DISPLAY]
                    .equals("true")) {
                mWeatherNote = new TextView(mContext);
                RelativeLayout.LayoutParams lpWeathertNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                int weatherX = Integer
                        .parseInt(mWeatherNoteValue[ClockProperty.WEATHER_NOTE_X]);
                if (weatherX > 0) {
                    lpWeathertNote
                            .setMargins(
                                    weatherX,
                                    Integer.parseInt(mWeatherNoteValue[ClockProperty.WEATHER_NOTE_Y]),
                                    0, 0);
                } else {
                    lpWeathertNote.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    mWeatherNote
                            .setY(Integer
                                    .parseInt(mWeatherNoteValue[ClockProperty.WEATHER_NOTE_Y]));
                }

                mWeatherNote.setLayoutParams(lpWeathertNote);
                mWeatherNote
                        .setTextSize(Float
                                .parseFloat(mWeatherNoteValue[ClockProperty.WEATHER_NOTE_SIZE]));
                mWeatherNote
                        .setTextColor(Color
                                .parseColor(mWeatherNoteValue[ClockProperty.WEATHER_NOTE_COLOR]));
                if (!mSunSetNoteValue[ClockProperty.WEATHER_NOTE_FONT]
                        .equals("")) {
                    try {
                        Typeface weekNoteFace = Typeface
                                .createFromFile(clock.getmFilePath()
                                        + "/"
                                        + mWeatherNoteValue[ClockProperty.WEATHER_NOTE_FONT]);
                        mWeatherNote.setTypeface(weekNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                mWeatherNote.setText(mWeatherInfo);
                mClockViewLayout.addView(mWeatherNote, lpWeathertNote);
            }
        }

        // 显示上午和下午的提示内容
        if (mAmPmNoteValues != null
                && mAmPmNoteValues.length > ClockProperty.AM_PM_NOTE_DISPLAY) {
            if (mAmPmNoteValues[ClockProperty.AM_PM_NOTE_DISPLAY]
                    .equals("true")) {
                mAnalogAmPmNoteView = new TextView(mContext);
                RelativeLayout.LayoutParams lpAmPmNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpAmPmNote
                        .setMargins(
                                Integer.parseInt(mAmPmNoteValues[ClockProperty.AM_PM_NOTE_X_VALUE]),
                                Integer.parseInt(mAmPmNoteValues[ClockProperty.AM_PM_NOTE_Y_VALUE]),
                                0, 0);
                mAnalogAmPmNoteView.setLayoutParams(lpAmPmNote);
                mAnalogAmPmNoteView
                        .setTextSize(Float
                                .parseFloat(mAmPmNoteValues[ClockProperty.AM_PM_NOTE_FONT_SIZE]));
                mAnalogAmPmNoteView
                        .setTextColor(Color
                                .parseColor(mAmPmNoteValues[ClockProperty.AM_PM_NOTE_FONT_COLOR]));
                if (!mAmPmNoteValues[ClockProperty.AM_PM_NOTE_FONT].equals("")) {
                    try {
                        Typeface weekNoteFace = Typeface
                                .createFromFile(clock.getmFilePath()
                                        + "/"
                                        + mAmPmNoteValues[ClockProperty.AM_PM_NOTE_FONT]);
                        mAnalogAmPmNoteView.setTypeface(weekNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                mClockViewLayout.addView(mAnalogAmPmNoteView, lpAmPmNote);
            }
        }

        // 显示日的提示内容
        if (mDayNoteValues != null
                && mDayNoteValues.length > ClockProperty.DAY_NOTE_DISPLAY) {

            if (mDayNoteValues[ClockProperty.DAY_NOTE_DISPLAY].equals("true")) {
                mAnalogDayNoteView = new TextView(mContext);
                try {
                    setBackgroundView(mAnalogDayNoteView,
                            mDayNoteValues[ClockProperty.DAY_NOTE_BG]);
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                RelativeLayout.LayoutParams lpDayNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpDayNote
                        .setMargins(
                                Integer.parseInt(mDayNoteValues[ClockProperty.DAY_NOTE_X_VALUE]),
                                Integer.parseInt(mDayNoteValues[ClockProperty.DAY_NOTE_Y_VALUE]),
                                0, 0);
                mAnalogDayNoteView.setLayoutParams(lpDayNote);
                mAnalogDayNoteView
                        .setTextSize(Float
                                .parseFloat(mDayNoteValues[ClockProperty.DAY_NOTE_FONT_SIZE]));
                mAnalogDayNoteView
                        .setTextColor(Color
                                .parseColor(mDayNoteValues[ClockProperty.DAY_NOTE_FONT_COLOR]));

                if (!mDayNoteValues[ClockProperty.DAY_NOTE_FONT].equals("")) {
                    try {
                        Typeface weekNoteFace = Typeface.createFromFile(clock
                                .getmFilePath()
                                + "/"
                                + mDayNoteValues[ClockProperty.DAY_NOTE_FONT]);
                        mAnalogDayNoteView.setTypeface(weekNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                try {
                    setBackgroundView(mAnalogDayNoteView,
                            mDayNoteValues[ClockProperty.DAY_NOTE_BG]);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    // e.printStackTrace();
                }
                mClockViewLayout.addView(mAnalogDayNoteView, lpDayNote);
            }
        }

        // 时的提示内容
        if (mHourLocation != null
                && mHourLocation.length > ClockProperty.HOUR_DISPLAY_BG) {
            if (mHourLocation[ClockProperty.HOUR_DISPLAY].equals("true")) {
                mAnalogHourNoteView = new TextView(mContext);
                try {
                    setBackgroundView(mAnalogHourNoteView,
                            mHourLocation[ClockProperty.HOUR_DISPLAY_BG]);
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                RelativeLayout.LayoutParams lpHourNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpHourNote
                        .setMargins(
                                Integer.parseInt(mHourLocation[ClockProperty.HOUR_X_VALUE]),
                                Integer.parseInt(mHourLocation[ClockProperty.HOUR_Y_VALUE]),
                                0, 0);
                mAnalogHourNoteView.setLayoutParams(lpHourNote);

                mAnalogHourNoteView
                        .setTextSize(Float
                                .parseFloat(mHourLocation[ClockProperty.HOUR_FONT_SIZE]));
                mAnalogHourNoteView
                        .setTextColor(Color
                                .parseColor(mHourLocation[ClockProperty.HOUR_FONT_COLOR]));

                if (!mHourLocation[ClockProperty.HOUR_FONT].equals("")) {
                    try {
                        Typeface hourNoteFace = Typeface.createFromFile(clock
                                .getmFilePath()
                                + "/"
                                + mHourLocation[ClockProperty.HOUR_FONT]);
                        mAnalogHourNoteView.setTypeface(hourNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                mClockViewLayout.addView(mAnalogHourNoteView, lpHourNote);

            }
        }

        // 分的提示内容
        if (mMinuteLocation != null
                && mMinuteLocation.length > ClockProperty.MINUTE_DISPLAY) {
            if (mMinuteLocation[ClockProperty.MINUTE_DISPLAY].equals("true")) {
                mAnalogMinuteNoteView = new TextView(mContext);
                try {
                    setBackgroundView(mAnalogMinuteNoteView,
                            mMinuteLocation[ClockProperty.MINUTE_DISPLAY_BG]);
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                RelativeLayout.LayoutParams lpMinuteNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpMinuteNote
                        .setMargins(
                                Integer.parseInt(mMinuteLocation[ClockProperty.MINUTE_X_VALUE]),
                                Integer.parseInt(mMinuteLocation[ClockProperty.MINUTE_Y_VALUE]),
                                0, 0);
                mAnalogMinuteNoteView.setLayoutParams(lpMinuteNote);

                mAnalogMinuteNoteView
                        .setTextSize(Float
                                .parseFloat(mMinuteLocation[ClockProperty.MINUTE_FONT_SIZE]));
                mAnalogMinuteNoteView
                        .setTextColor(Color
                                .parseColor(mMinuteLocation[ClockProperty.MINUTE_FONT_COLOR]));

                if (!mMinuteLocation[ClockProperty.MINUTE_FONT].equals("")) {
                    try {
                        Typeface MinuteNoteFace = Typeface.createFromFile(clock
                                .getmFilePath()
                                + "/"
                                + mMinuteLocation[ClockProperty.MINUTE_FONT]);
                        mAnalogMinuteNoteView.setTypeface(MinuteNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                mClockViewLayout.addView(mAnalogMinuteNoteView, lpMinuteNote);
            }
        }

        // 显示周的提示内容
        if (mWeekNoteValue != null
                && mWeekNoteValue.length > ClockProperty.WEEK_NOTE_DISPLAY) {
            if (mWeekNoteValue[ClockProperty.WEEK_NOTE_DISPLAY].equals("true")) {
                mAnalogWeekNoteView = new TextView(mContext);
                RelativeLayout.LayoutParams lpWeekNote = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lpWeekNote
                        .setMargins(
                                Integer.parseInt(mWeekNoteValue[ClockProperty.WEEK_NOTE_X_VALUE]),
                                Integer.parseInt(mWeekNoteValue[ClockProperty.WEEK_NOTE_Y_VALUE]),
                                0, 0);
                mAnalogWeekNoteView.setLayoutParams(lpWeekNote);
                mAnalogWeekNoteView
                        .setTextSize(Float
                                .parseFloat(mWeekNoteValue[ClockProperty.WEEK_NOTE_FONT_SIZE]));
                mAnalogWeekNoteView
                        .setTextColor(Color
                                .parseColor(mWeekNoteValue[ClockProperty.WEEK_NOTE_FONT_COLOR]));
                if (!mWeekNoteValue[ClockProperty.WEEK_NOTE_FONT].equals("")) {
                    try {
                        Typeface weekNoteFace = Typeface.createFromFile(clock
                                .getmFilePath()
                                + "/"
                                + mWeekNoteValue[ClockProperty.WEEK_NOTE_FONT]);
                        mAnalogWeekNoteView.setTypeface(weekNoteFace);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                mClockViewLayout.addView(mAnalogWeekNoteView, lpWeekNote);
            }
        }


        String[] timeSeparted = null;
        if (clock.getTimeSelocation() != null) {
            timeSeparted = clock.getTimeSelocation().split(",");
        }

        if (timeSeparted != null
                && timeSeparted.length > ClockProperty.TIME_NOTE_FONT) {
            mTimeSeparatedNote = new TextView(mContext);
            RelativeLayout.LayoutParams lpTimeSeparate = new RelativeLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            lpTimeSeparate
                    .setMargins(
                            Integer.parseInt(timeSeparted[ClockProperty.TIME_NOTE_X_VALUE]),
                            Integer.parseInt(timeSeparted[ClockProperty.TIME_NOTE_Y_VALUE]),
                            0, 0);
            mTimeSeparatedNote.setLayoutParams(lpTimeSeparate);
            mTimeSeparatedNote
                    .setTextSize(Float
                            .parseFloat(timeSeparted[ClockProperty.TIME_NOTE_FONT_SIZE]));
            mTimeSeparatedNote
                    .setTextColor(Color
                            .parseColor(timeSeparted[ClockProperty.TIME_NOTE_FONT_COLOR]));
            if (timeSeparted.length > ClockProperty.TIME_NOTE_FONT
                    && !timeSeparted[ClockProperty.TIME_NOTE_FONT].equals("")) {
                try {
                    Typeface timeNoteFace = Typeface.createFromFile(clock
                            .getmFilePath()
                            + "/"
                            + timeSeparted[ClockProperty.TIME_NOTE_FONT]);
                    mTimeSeparatedNote.setTypeface(timeNoteFace);
                } catch (Exception e) {
                    // TODO: handle exception
                }
            } else {
                Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                        "fonts/dclassic.ttf");
                mTimeSeparatedNote.setTypeface(tf);
            }
            mTimeSeparatedNote.setText(mContext
                    .getString(R.string.time_separated));
            mClockViewLayout.addView(mTimeSeparatedNote, lpTimeSeparate);
        }

        // 添加表盘名称
        if (mClockNameValue != null
                && mClockNameValue.length > ClockProperty.CLOCK_DISPALY_NAME_COLOR) {

            mAnalogClockNameView
                    .setTextSize(Integer
                            .parseInt(mClockNameValue[ClockProperty.CLOCK_DISPALY_NAME_SIZE]));
            mAnalogClockNameView
                    .setTextColor(Color
                            .parseColor(mClockNameValue[ClockProperty.CLOCK_DISPALY_NAME_COLOR]));
            mAnalogClockNameView.setText(ClockUtils.getLocalClockName(
                    mClockNameValue, mContext));

        }

        if (clock.getmHourProgress() != null
                && !clock.getmHourProgress().equals("")) {
            String pointHourProgress[] = clock.getmHourProgress().split(",");

            mPointHourProgress = new PointHourProgress(mContext, clock);
            LayoutParams lpHour = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);

            if (pointHourProgress[ClockProperty.POINT_PROGRESS_TYPE] != null
                    && !pointHourProgress[ClockProperty.POINT_PROGRESS_TYPE]
                            .equals("")) {
                mPointHourProgress
                        .setStyle(ClockUtils
                                .getPointProgressType(pointHourProgress[ClockProperty.POINT_PROGRESS_TYPE]));
            }

            if (pointHourProgress[ClockProperty.POINT_PROGRESS_START_COLOR] != null
                    && !pointHourProgress[ClockProperty.POINT_PROGRESS_START_COLOR]
                            .equals("")) {
                mPointHourProgress
                        .setRoundProgressStartColor(Color
                                .parseColor(pointHourProgress[ClockProperty.POINT_PROGRESS_START_COLOR]));
            }

            if (pointHourProgress[ClockProperty.POINT_PROGRESS_END_COLOR] != null
                    && !pointHourProgress[ClockProperty.POINT_PROGRESS_END_COLOR]
                            .equals("")) {
                mPointHourProgress
                        .setRoundProgressFinishColor(Color
                                .parseColor(pointHourProgress[ClockProperty.POINT_PROGRESS_END_COLOR]));
            }

            mPointHourProgress.setOuterRadiusHeightRatio(0.016f);
            mPointHourProgress.setTextIsDisplayable(false);

            mClockViewLayout.addView(mPointHourProgress, lpHour);

        }

        if (clock.getmHourPoint() != null && !clock.getmHourPoint().equals("")) {

            mPointHour = new PointHourView(mContext, clock);
            LayoutParams lpHour = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);
            mClockViewLayout.addView(mPointHour, lpHour);
        }

        if (clock.getmMinuteProgress() != null
                && !clock.getmMinuteProgress().equals("")) {
            String pointMinuteProgress[] = clock.getmMinuteProgress()
                    .split(",");

            mPointMinuteProgress = new PointMinuteProgress(mContext, clock);
            LayoutParams lpMinute = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);

            if (pointMinuteProgress[ClockProperty.POINT_PROGRESS_TYPE] != null
                    && !pointMinuteProgress[ClockProperty.POINT_PROGRESS_TYPE]
                            .equals("")) {
                mPointMinuteProgress
                        .setStyle(ClockUtils
                                .getPointProgressType(pointMinuteProgress[ClockProperty.POINT_PROGRESS_TYPE]));
            }

            if (pointMinuteProgress[ClockProperty.POINT_PROGRESS_START_COLOR] != null
                    && !pointMinuteProgress[ClockProperty.POINT_PROGRESS_START_COLOR]
                            .equals("")) {
                mPointMinuteProgress
                        .setRoundProgressStartColor(Color
                                .parseColor(pointMinuteProgress[ClockProperty.POINT_PROGRESS_START_COLOR]));
            }

            if (pointMinuteProgress[ClockProperty.POINT_PROGRESS_END_COLOR] != null
                    && !pointMinuteProgress[ClockProperty.POINT_PROGRESS_END_COLOR]
                            .equals("")) {
                mPointMinuteProgress
                        .setRoundProgressFinishColor(Color
                                .parseColor(pointMinuteProgress[ClockProperty.POINT_PROGRESS_END_COLOR]));
            }

            mPointMinuteProgress.setOuterRadiusHeightRatio(0.016f);
            mPointMinuteProgress.setTextIsDisplayable(false);

            mClockViewLayout.addView(mPointMinuteProgress, lpMinute);

        }

        if (clock.getmMinutePoint() != null
                && !clock.getmMinutePoint().equals("")) {

            mPointMinute = new PointMinuteView(mContext, clock);
            LayoutParams lpMinute = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);
            mClockViewLayout.addView(mPointMinute, lpMinute);
        }

        if (clock.getmSecondProgress() != null
                && !clock.getmSecondProgress().equals("")) {
            String pointSecondProgress[] = clock.getmSecondProgress()
                    .split(",");

            mPointSecondProgress = new PointSecondProgress(mContext, clock);
            LayoutParams lpSecond = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);

            if (pointSecondProgress[ClockProperty.POINT_PROGRESS_TYPE] != null
                    && !pointSecondProgress[ClockProperty.POINT_PROGRESS_TYPE]
                            .equals("")) {

                if (pointSecondProgress[ClockProperty.POINT_PROGRESS_TYPE]
                        .equals(ClockProperty.POINT_LINK_STROKE)) {
                    mPointSecondProgress.setStyle(ClockProperty.LINK_STROKE);
                    mPointSecondProgress.setOuterRadiusHeightRatio(0.016f);
                    mPointSecondProgress.setTextIsDisplayable(false);
                } else if (pointSecondProgress[ClockProperty.POINT_PROGRESS_TYPE]
                        .equals(ClockProperty.POINT_SPACE_SMALL)) {
                    mPointSecondProgress.setStyle(ClockProperty.SPACE_SMALL);
                    mPointSecondProgress.setOuterRadiusHeightRatio(0.07f);
                    mPointSecondProgress.setHeightWidthRatio(0.9f);
                    mPointSecondProgress.setTextIsDisplayable(false);
                } else if (pointSecondProgress[ClockProperty.POINT_PROGRESS_TYPE]
                        .equals(ClockProperty.POINT_SPACE_BIG)) {
                    mPointSecondProgress.setStyle(ClockProperty.SPACE_BIG);
                    mPointSecondProgress.setOuterRadiusHeightRatio(0.10f);
                    mPointSecondProgress.setHeightWidthRatio(5.0f);
                    mPointSecondProgress.setTextIsDisplayable(false);
                }

            }

            if (pointSecondProgress[ClockProperty.POINT_PROGRESS_START_COLOR] != null
                    && !pointSecondProgress[ClockProperty.POINT_PROGRESS_START_COLOR]
                            .equals("")) {
                mPointSecondProgress
                        .setRoundProgressStartColor(Color
                                .parseColor(pointSecondProgress[ClockProperty.POINT_PROGRESS_START_COLOR]));
            }

            if (pointSecondProgress[ClockProperty.POINT_PROGRESS_END_COLOR] != null
                    && !pointSecondProgress[ClockProperty.POINT_PROGRESS_END_COLOR]
                            .equals("")) {
                mPointSecondProgress
                        .setRoundProgressFinishColor(Color
                                .parseColor(pointSecondProgress[ClockProperty.POINT_PROGRESS_END_COLOR]));
            }

            mClockViewLayout.addView(mPointSecondProgress, lpSecond);

        }

        if (clock.getmSecondPoint() != null
                && !clock.getmSecondPoint().equals("")) {

            mPointSeconds = new PointSecondsView(mContext, clock);
            LayoutParams lpSeconds = new LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            mClockViewLayout.addView(mPointSeconds, lpSeconds);
        }

        updateAmPmDayWeekNote();
    }

    private int mDayOld = -1;

    @SuppressWarnings("deprecation")
    private void updateAmPmDayWeekNote() {
        String[] preClock = null;
        if (clock.getmPreclock() != null
                && mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {
            preClock = clock.getmPreclock().split(",");
        }

        Calendar mClockCalendar = Calendar.getInstance();
        mClockCalendar.setTimeInMillis(System.currentTimeMillis());
        if (!TextUtils.isEmpty(mTimeZoneId)) {
            mClockCalendar.setTimeZone(TimeZone.getTimeZone(mTimeZoneId));
        }
        // 显示月相
        if (mMoonNameView != null) {
            @SuppressWarnings("deprecation")
            int mCurrentDay = mClockCalendar.getTime().getDay();
            if (mCurrentDay != mDayOld) {
                // 时间有改变
                ChineseLunar mIngenicLunar = new ChineseLunar(mClockCalendar);
                int moonLevel = ClockUtils.getMoonIndex(mIngenicLunar
                        .getChinaDay());
                String moonname = ClockUtils.getMoonName(moonLevel, mContext);
                mMoonNameView.setText(moonname);
                // 显示月相的背景
                if (mMoonViewBg != null) {
                    mMoonViewBg.setBackground(new BitmapDrawable(ClockUtils
                            .getMoonIndxeBitmap(moonLevel, clock)));
                }
                mDayOld = mCurrentDay;
            }
        }


        // 显示日出，日落的时间
        if (mSunRiseNote != null ) {
            mSunRiseNote.setText(ClockUtils.getSunRiseSetTimeString(mSunRise));
        }

        if (mSunSetNote != null) {
            mSunSetNote.setText(ClockUtils.getSunRiseSetTimeString(mSunSet));
        }

        String hour = DateFormat.format("kk", mClockCalendar).toString();
        String minute = DateFormat.format("mm", mClockCalendar).toString();
        if (mDisplayNowTimeState == ClockProperty.CLOCK_SCALE_SMALL) {

            if (preClock != null
                    && preClock.length > ClockProperty.PRE_CLOCK_HOUR) {
                if (mAnalogHourNoteView != null) {
                    mAnalogHourNoteView
                            .setText(ClockUtils.formatClockTime(Integer
                                    .parseInt(preClock[ClockProperty.PRE_CLOCK_HOUR])));
                }
            }

            if (preClock != null
                    && preClock.length > ClockProperty.PRE_CLOCK_MINUTE) {
                if (mAnalogMinuteNoteView != null) {
                    mAnalogMinuteNoteView
                            .setText(ClockUtils.formatClockTime(Integer
                                    .parseInt(preClock[ClockProperty.PRE_CLOCK_MINUTE])));
                }
            }

        } else {

            if (mAnalogHourNoteView != null) {
                mAnalogHourNoteView.setText(hour + "");
            }

            if (mAnalogMinuteNoteView != null) {
                mAnalogMinuteNoteView.setText(minute + "");
            }
        }

        if (mAnalogWeekNoteView != null)
            mAnalogWeekNoteView.setText(getDayOfWeek(mClockCalendar
                    .get(Calendar.DAY_OF_WEEK)));

        if (mAnalogDayNoteView != null) {
            if (mDayNoteValues.length > ClockProperty.DAY_NOTE_TYPE) {

                if (mDayNoteValues[ClockProperty.DAY_NOTE_TYPE].toString()
                        .equals(ClockProperty.DAY_TEYP_DAY_MONTH)) {
                    mAnalogDayNoteView.setText(String.format("%02d",
                            mClockCalendar.get(Calendar.MONTH))
                            + "/"
                            + String.format("%02d",
                                    mClockCalendar.get(Calendar.DATE)) + "");
                }
            } else {
                mAnalogDayNoteView.setText(String.format("%02d",
                        mClockCalendar.get(Calendar.DATE))
                        + "");
            }
        }
        if (mAnalogAmPmNoteView != null) {
            int amPm = mClockCalendar.get(Calendar.AM_PM); // 0
            if (amPm == 0) {
                mAnalogAmPmNoteView.setText("AM");
            } else {
                mAnalogAmPmNoteView.setText("PM");
            }
        }
    }

    private String getDayOfWeek(int dayOfWeek) {
        String[] days = ((Activity) mContext).getResources().getStringArray(
                R.array.library_day_of_week_entries);
        if (dayOfWeek < 0 || dayOfWeek > days.length) {
            return days[0];
        } else {
            return days[dayOfWeek - 1];
        }
    }

    @SuppressWarnings("deprecation")
    private void setBackgroundView(View view, String bgPath) throws IOException {
        if (clock == null)
            return;
        if (!"".equals(clock.getmBackground())) {
            FileInputStream input;
            Drawable mBackground = null;
            try {
                input = new FileInputStream(new File(clock.getmFilePath()
                        + File.separator + bgPath));

                Bitmap bmp = BitmapFactory
                        .decodeStream(new BufferedInputStream(input));
                mBackground = new BitmapDrawable(bmp);
                input.close();
                view.setBackground(mBackground);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                // e.printStackTrace();
            }
        }
    }

    private String mTimeZoneId;

    public void setTimeZone(String id) {
        mTimeZoneId = id;
        updateTime();
    }

    public void recyCleBitmap() {
        recycleBitmap();
        if (mPointHour != null) {
            mPointHour.recycleBitmap();
        }

        if (mPointMinute != null) {
            mPointMinute.recycleBitmap();
        }

        if (mPointSeconds != null) {
            mPointSeconds.recycleBitmap();
        }

        if (mMonthView != null) {
            mMonthView.recycleBitmap();
        }
        if (mWeekView != null) {
            mWeekView.recycleBitmap();
        }
        if (mAmPmView != null) {
            mAmPmView.recycleBitmap();
        }
    }

    @Override
    public void updateTime() {
        updateAmPmDayWeekNote();

        if (mPointHourProgress != null) {
            mPointHourProgress.updateTime(mTimeZoneId);
        }

        if (mPointMinuteProgress != null) {
            mPointMinuteProgress.updateTime(mTimeZoneId);
        }

        if (mPointSecondProgress != null) {
            mPointSecondProgress.updateTime(mTimeZoneId);
        }

        if (mPointHour != null) {
            mPointHour.updateTime(mTimeZoneId);
        }
        if (mPointMinute != null) {
            mPointMinute.updateTime(mTimeZoneId);
        }

        if (mPointSeconds != null) {
            mPointSeconds.updateTime(mTimeZoneId);
        }

        if (mMonthView != null) {
            mMonthView.updateTime(mTimeZoneId);
        }
        if (mWeekView != null) {
            mWeekView.updateTime(mTimeZoneId);
        }
        if (mAmPmView != null) {
            mAmPmView.updateTime(mTimeZoneId);
        }
    }
}
