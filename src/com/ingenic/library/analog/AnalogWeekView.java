/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingClock Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.library.analog;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.format.Time;
import android.view.View;

import com.ingenic.library.clock.Clock;

/**
 * 显示周view
 *
 * @author ShiGuangHua(Kenny)
 *
 */
public class AnalogWeekView extends View {
    public static final boolean DEBUG = false;
    private boolean mAttached = true;
    protected Time mCalendar;
    protected Drawable mMonthHand = null;
    private Bitmap mBitmap = null;
    static protected float mWeek;
    protected int mDialWidth;
    protected int mDialHeight;
    protected boolean mChanged;
    private Clock mClock;

    /**
     * @param context
     */
    public AnalogWeekView(Context context) {
        super(context);
        setWillNotDraw(false);
    }

    public AnalogWeekView(Context context, Clock clock) {
        this(context);
        mClock = clock;
        recycleBitmap();
        mCalendar = new Time();
        String clockPath = mClock.getmFilePath();
        setFiles(new File(clockPath + "/" + mClock.getmWeekPoint()));
    }

    private String mTimeZoneId;

    public void updateTime(String id) {
        mTimeZoneId = id;
        onTimeChanged();
        invalidate();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!mAttached) {
            mAttached = true;
        }
    }

    public synchronized void recycleBitmap() {
        if (mBitmap != null && !mBitmap.isRecycled()) {
            mBitmap.recycle();
            mBitmap = null;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mAttached) {
            mAttached = false;
        }
    }

    private void getCurrentTime() {
        mCalendar.setToNow();
        if (mTimeZoneId != null) {
            mCalendar.switchTimezone(mTimeZoneId);
        }
        float week = mCalendar.weekDay;
        mWeek = week;
    }

    private void onTimeChanged() {
        float oldWeek = mWeek;
        getCurrentTime();
        mChanged = oldWeek != mWeek;
    }

    @SuppressWarnings("deprecation")
    public void setFiles(File monthFile) {
        try {
            FileInputStream inputHour = new FileInputStream(monthFile);
            mBitmap = BitmapFactory.decodeStream(new BufferedInputStream(
                    inputHour));
            mMonthHand = new BitmapDrawable(mBitmap);
            inputHour.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    protected void onDraw(Canvas canvas) {
        getCurrentTime();
        int availableWidth = getRight() - getLeft();
        int availableHeight = getBottom() - getTop();
        int x = availableWidth / 2;
        int y = availableHeight / 2;
        int w, h;
        canvas.save();
        if (mWeek == 0) {
            mWeek = 7;
        }
        canvas.rotate(mWeek / 7.0f * 360.0f, x, y);
        if (mMonthHand == null)
            return;
        final Drawable monthHand = mMonthHand;
        w = (int) ((monthHand.getIntrinsicWidth()) * 1.5);
        h = (int) ((monthHand.getIntrinsicHeight()) * 1.5);
        monthHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y + (h / 2));
        monthHand.draw(canvas);
        canvas.restore();
    }
}
